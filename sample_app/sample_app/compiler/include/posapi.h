/*****************************************************/
/* posapi.h                                             */
/* Define the Application Program Interface          */
/* for all PAX POS terminals     		     */
/*****************************************************/

#ifndef  _PAX_POS_API_H
#define  _PAX_POS_API_H
//Standard header files
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>

#if defined(__GNUC__)
	#define APPINFO_SECTION __attribute__((used, section(".appinfo"), \
	aligned(sizeof(long))))
#else
	#define APPINFO_SECTION
#endif

//#pragma warning( disable : 4005 )
#ifndef _PROPAY_
	#define SEEK_CUR	0
	#define	SEEK_SET	1
	#define	SEEK_END	2
#endif	// #ifdef _PROPAY_
#define uchar unsigned char
#define uint unsigned int
#define ulong unsigned long
//#define ushort unsigned short
//#pragma warning( default : 4005 )


/*monitor logic dir*/
#define APP_DATA_DIR 	"/appdata"
#define PUBLIC_DIR 		"/public"
#define SDCARD_DIR  	"/sdcard"
#define USB_DIR 		"/usbstorage"


////兼容GNUC
#ifdef PROLIN_OS_APP
	#pragma pack(push)
	#pragma pack(1)
#endif

//============================================
//      key values
//============================================
#define KEYF1       0x01
#define KEYF2       0x02
#define KEYF3       0x03
#define KEYF4       0x04
#define KEYUP       0x05
#define KEYDOWN     0x06
#define KEYALPHA    0x07
#define KEYCLEAR    0x08
#define KEYF5       0x09
#define KEYBACKSPACE  0x09
#define KEYF6       0x0a
#define KEYENTER    0x0d
#define KEYMENU     0x14
#define KEYFN       0x15
#define KEYSETTLE   0x16
#define KEYVOID     0x17
#define KEYREPRINT  0x18
#define KEYPRNUP    0x19
#define KEYPRNDOWN  0x1a
#define KEYCANCEL   0x1b
#define KEY00       0x3a

#define KEY0        0x30
#define KEY1        0x31
#define KEY2        0x32
#define KEY3        0x33
#define KEY4        0x34
#define KEY5        0x35
#define KEY6        0x36
#define KEY7        0x37
#define KEY8        0x38
#define KEY9        0x39

#define KEYATM1     0x3B
#define KEYATM2     0x3C
#define KEYATM3     0x3D
#define KEYATM4     0x3E

#define NOKEY       0xff

#define FNKEY1      0x91
#define FNKEY2      0x92
#define FNKEY3      0x93
#define FNKEY4      0x94
#define FNKEY5      0x95
#define FNKEY6      0x96
#define FNKEY7      0x97
#define FNKEY8      0x98
#define FNKEY9      0x99
#define FNKEY0      0x90

#define FNKEYCLEAR  0x9a
#define FNKEYALPHA  0x9b
#define FNKEYUP     0x9c
#define FNKEYDOWN   0x9d
#define FNKEYMENU   0x9e
#define FNKEYENTER  0x9f
#define FNKEYCANCEL 0xa0
#define FNKEYF2     0xa5

#define KEYSTAR  	0X2A  //*
#define KEYNUM  	0X23  //#
#define KEYFLASH  	0X8B  //闪断/返回
#define KEYRD  		0X8C  //重拨/暂停
#define KEYHF  		0X8D  //免提



#define KEYADD 0X2B
#define KEYSUB 0X2D
#define KEYMUL 0X26
#define KEYDOT 0X2E
#define FNKEYADD 0XBB
#define FNKEYSUB 0XBD
#define FNKEYMUL 0XB6
#define FNKEYDOT 0XBE


//================================================
//              Define for the Power Save event
//================================================
#define EVENT_OVERTIME      0X01
#define EVENT_KEYPRESS      0X02
#define EVENT_MAGSWIPED     0X04
#define EVENT_ICCIN         0X08
#define EVENT_UARTRECV      0X10
#define EVENT_WNETRECV      0X20

//==================================================
//		MACROS for LCD functions
//===================================================
#define ASCII		0x00
#define CFONT		0x01
#define REVER		0x80


#define ICON_PHONE	   1	// phone 电话
#define ICON_SIGNAL	   2	// wireless signal 信号
#define ICON_PRINTER   3	// printer 打印机
#define ICON_ICCARD	   4	// smart card IC卡
#define ICON_LOCK      5	// lock 锁
#define ICON_SPEAKER   6	// speeker 扬声器
#define ICON_BATTERY   6	// 电池图标
#define ICON_UP        7	// up 向上
#define ICON_DOWN      8	// down 向下

#define ICON_NET       9    //以太网网口通断指示图标  MT30(博通平台)
#define ICON_USB       10   /*   usb */
#define ICON_BT        11   
#define ICON_WIFI	   12
#define ICON_LAN       13
#define ICON_NUMBER    14

#define CLOSEICON	   0	// 关闭图标[针对所有图标]
#define OPENICON       1   // 显示图标[针对打印机、IC卡、锁、电池、向上、向下]


//===========================================================
//           structure for smart card operation
//===========================================================
typedef struct{
   	unsigned char Command[4];
   	unsigned short Lc;
   	unsigned char  DataIn[512];
   	unsigned short Le;
}APDU_SEND;

typedef struct{
	unsigned short LenOut;
   	unsigned char  DataOut[512];
   	unsigned char  SWA;
   	unsigned char  SWB;
}APDU_RESP;


//================================================
//         structure for modem operation
//=================================================
typedef struct{
	unsigned char DP;
	unsigned char CHDT;
	unsigned char DT1;
	unsigned char DT2;
	unsigned char HT;
	unsigned char WT;
	unsigned char SSETUP;
	unsigned char DTIMES;
	unsigned char TimeOut;
	unsigned char AsMode;
}COMM_PARA;


typedef struct
{
         /** 密钥结构体的用法类型:
                   0x00 - 同PedWriteKey
                   0x01 - 民生银行国密改造，支持源密钥与目的密钥不同算法，
                            并且满足密钥补位规则*/
         int iFormat;
         /** 发散该密钥的源密钥的密钥类型，
         PED_TMK,PED_TPK,PED_TAK,PED_TDK, 
         不得低于ucDstKeyType所在的密钥级别*/
         unsigned char ucSrcKeyType;
         /** 发散该密钥的源密钥索引，
         索引一般从1开始，如果该变量为0，
         则表示这个密钥的写入是明文形式*/
         unsigned char ucSrcKeyIdx;
         /** 目的密钥的密钥类型，
         PED_TMK,PED_TPK,PED_TAK,PED_TDK 
         */
         unsigned char ucDstKeyType;
         /*目的密钥索引*/
         unsigned char ucDstKeyIdx;
         /**明文目的密钥长度，8,16,24*/
         int iDstKeyLen;
         /**密文目的密钥长度，8,16,24,32*/
         int iCipherKeyLen;
         //写入密钥的内容
         unsigned char aucDstKeyValue[32];
} ST_KEY_INFO_EX;

//=============================================================================
// structure  and macros for ped operation begin
// nicm add 2008-07-25 being
//=============================================================================
typedef struct
{
	// 发散该密钥的源密钥的密钥类型，PED_GMK,PED_TMK,PED_TPK,PED_TAK,PED_TDK, 不得低于ucDstKeyType所在的密钥级别
	uchar ucSrcKeyType; 
	// 发散该密钥的源密钥索引，索引一般从1开始，如果该变量为0，则表示这个密钥的写入是明文形式
	uchar ucSrcKeyIdx; 
	// 目的密钥的密钥类型，PED_GMK,PED_TMK,PED_TPK,PED_TAK,PED_TDK
	uchar ucDstKeyType;
	// 目的密钥索引
	uchar ucDstKeyIdx;
	// 目的密钥长度，8,16,24
	int iDstKeyLen; 
	// 写入密钥的内容
	uchar aucDstKeyValue[24]; 
}ST_KEY_INFO;

typedef struct
{
  int iCheckMode;
  uchar aucCheckBuf[128];
}ST_KCV_INFO;

#define __0__PED_DEFINE__
#define PED_BKLK  0x01  //BKLK
#define	PED_DUKPT 0X06
#define	PED_TPAK  0X07//(用于TPK/TAK混用的模式)

//============================================
//      PED key Type 密钥类型
//============================================
#define PED_TLK 			0x01		//Loading Key
#define PED_TMK 			0x02		//Master Key
#define PED_TPK 			0x03		//PIN Key
#define PED_TAK 			0x04		//MAC Key
#define PED_TDK 			0x05		//only for data decrypt key,
#define	PED_TEK				0X06 		//only for data encrypte key
#define PED_TIK 			0x07		//DUKPT Key	
#define PED_TRK 			0x08 		//msr key
#define PED_TADK 			0x0F
#define TMP_KEK				0x80 		//TEMP KEY FOR DOWNLOAD TLK
#define PED_TMEK 			0x10
#define PED_TAESK			0x20		//AES Key
#define PED_SM2_PVT_KEY		0x30		//SM2 Private Key
#define PED_SM2_PUB_KEY		0x31		//SM2 Public Key
#define PED_SM4_TMK			0x32		//SM4 Master Key
#define PED_SM4_TPK			0x33		//SM4 PIN Key
#define PED_SM4_TAK			0x34		//SM4 MAC Key	
#define PED_SM4_TDK			0x35		//SM4 Data Key
#define PED_PPAD_TXK		0x42		//Exchange Key for external pinpad
#define PED_TIDK        	0x45  		//identity key
#define PED_DECRYPT 		0x00
#define PED_ENCRYPT 		0x01

#define PED_ECB_DECRYPT 	0x00
#define PED_ECB_ENCRYPT 	0x01
#define PED_CBC_DECRYPT 	0x02
#define PED_CBC_ENCRYPT 	0x03

//define密钥下载命令码定义
#define PED_DOWNLOAD_CMD_TYPE 0X00
// 读取版本
#define PED_CMD_GETVER 0x00
// 请求系统敏感服务授权并读取随机数
#define PED_CMD_REQ_SYS_SSA	0X01
// 回送随机数密文并获取
#define PED_CMD_GET_TEK 0X02
// 格式化ped
#define PED_CMD_FORMAT_PED 0X03//
 // 写入MKSK 密钥
#define PED_CMD_WRITEKEY 0x04//
// 写入TIK
#define PED_CMD_WRITETIK 0x05//
// 设置密钥标签
#define PED_CMD_SETKEYTAG 0X06//
// PED下载完成
#define PED_CMD_DOWNLOAD_COMPLETE 0X07	
 // 读取终端序列号
#define PED_CMD_READ_SN 0X08

// PEDAPI 错误码起始值
#define PED_RET_ERR_START -300
// PEDAPI 错误码结束值
#define PED_RET_ERR_END	-500
// PED函数返回正确
#define PED_RET_OK 0  
// 密钥不存在
#define PED_RET_ERR_NO_KEY (PED_RET_ERR_START-1)  
// 密钥索引错，参数索引不在范围内
#define PED_RET_ERR_KEYIDX_ERR     (PED_RET_ERR_START-2)  
// 密钥写入时，源密钥的层次比目的密钥低
#define PED_RET_ERR_DERIVE_ERR	   (PED_RET_ERR_START-3)  
// 密钥验证失败
#define PED_RET_ERR_CHECK_KEY_FAIL (PED_RET_ERR_START-4)  
// 没输入PIN
#define PED_RET_ERR_NO_PIN_INPUT   (PED_RET_ERR_START-5) 
// 取消输入PIN
#define PED_RET_ERR_INPUT_CANCEL   (PED_RET_ERR_START-6) 
// 函数调用小于最小间隔时间
#define PED_RET_ERR_WAIT_INTERVAL  (PED_RET_ERR_START-7) 
 // KCV模式错，不支持
#define PED_RET_ERR_CHECK_MODE_ERR (PED_RET_ERR_START-8) 
// 无权使用该密钥，当出现密钥标签不对，或者写入密钥时,
// 源密钥类型的值大于目的密钥类型，都会返回该密钥
#define PED_RET_ERR_NO_RIGHT_USE   (PED_RET_ERR_START-9) 
// 密钥类型错
#define PED_RET_ERR_KEY_TYPE_ERR   (PED_RET_ERR_START-10)  
// 期望PIN的长度字符串错
#define PED_RET_ERR_EXPLEN_ERR     (PED_RET_ERR_START-11)  
// 目的密钥索引错，不在范围内
#define PED_RET_ERR_DSTKEY_IDX_ERR (PED_RET_ERR_START-12)  
// 源密钥索引错，不在范围内
#define PED_RET_ERR_SRCKEY_IDX_ERR (PED_RET_ERR_START-13)  
// 密钥长度错
#define PED_RET_ERR_KEY_LEN_ERR		(PED_RET_ERR_START-14) 
// 输入PIN超时
#define PED_RET_ERR_INPUT_TIMEOUT  (PED_RET_ERR_START-15) 
// IC卡不存在
#define PED_RET_ERR_NO_ICC          (PED_RET_ERR_START-16)
// IC卡未初始化
#define PED_RET_ERR_ICC_NO_INIT    (PED_RET_ERR_START-17) 
// DUKPT组索引号错
#define PED_RET_ERR_GROUP_IDX_ERR (PED_RET_ERR_START-18)
// 指针参数非法为空
#define PED_RET_ERR_PARAM_PTR_NULL    (PED_RET_ERR_START-19)
// PED已锁
#define PED_RET_ERR_LOCKED (PED_RET_ERR_START-20)		
// PED通用错误
#define PED_RET_ERROR (PED_RET_ERR_START-21)		
// 没有空闲的缓冲
#define PED_RET_ERR_NOMORE_BUF (PED_RET_ERR_START-22)	
// 需要取得高级权限
#define PED_RET_ERR_NEED_ADMIN (PED_RET_ERR_START-23)	
// DUKPT已经溢出
#define PED_RET_ERR_DUKPT_OVERFLOW (PED_RET_ERR_START-24)
// KCV 校验失败
#define PED_RET_ERR_KCV_CHECK_FAIL (PED_RET_ERR_START-25)
// 写入密钥时，源密钥id的类型不和源密钥类型不匹配
#define PED_RET_ERR_SRCKEY_TYPE_ERR	(PED_RET_ERR_START-26)
// 命令不支持
#define PED_RET_ERR_UNSPT_CMD  (PED_RET_ERR_START-27)
// 通讯错误
#define PED_RET_ERR_COMM_ERR (PED_RET_ERR_START-28)
// 没有用户认证公钥
#define PED_RET_ERR_NO_UAPUK (PED_RET_ERR_START-29)
// 取系统敏感服务失败
#define PED_RET_ERR_ADMIN_ERR (PED_RET_ERR_START-30)
// PED处于下载非激活状态
#define	PED_RET_ERR_DOWNLOAD_INACTIVE (PED_RET_ERR_START-31)

//KCV 奇校验失败
#define PED_RET_ERR_KCV_ODD_CHECK_FAIL (PED_RET_ERR_START-32)
//读取PED数据失败
#define PED_RET_ERR_PED_DATA_RW_FAIL (PED_RET_ERR_START-33)
//卡操作错误(脱机明文、密文密码验证)
#define PED_RET_ERR_ICC_CMD_ERR      (PED_RET_ERR_START-34)
//用户按CLEAR键退出输入PIN
#define PED_RET_ERR_INPUT_CLEAR		(PED_RET_ERR_START-39)
//PED存储空间不足
#define PED_RET_ERR_NO_FREE_FLASH	(PED_RET_ERR_START-43)
//DUKPT KSN需要先加1
#define PED_RET_ERR_DUKPT_NEED_INC_KSN	(PED_RET_ERR_START-44)
//KCV MODE错误
#define PED_RET_ERR_KCV_MODE_ERR	(PED_RET_ERR_START-45)
//NO KCV
#define PED_RET_ERR_DUKPT_NO_KCV   (PED_RET_ERR_START-46)
//按FN/ATM4键取消PIN输入
#define PED_RET_ERR_PIN_BYPASS_BYFUNKEY		(PED_RET_ERR_START-47)
//数据MAC校验错
#define PED_RET_ERR_MAC_ERR			(PED_RET_ERR_START-48)
//数据CRC校验错
#define PED_RET_ERR_CRC_ERR			(PED_RET_ERR_START-49)
//无效错误或无效
#define PED_RET_ERR_PARAM_INVALID   (PED_RET_ERR_START-50)
//=============================================================================
#define PED_RET_ERR_RKI_START -380
#define PED_RET_ERR_RKI_DATA_CHECK_FAIL PED_RET_ERR_RKI_START-1 //!<RKI数据检查失败
#define PED_RET_ERR_RKI_DEV_AUTH_FAIL PED_RET_ERR_RKI_START-2 //!<RKI认证失败
#define PED_RET_ERR_RKI_GEN_ATH_FAIL PED_RET_ERR_RKI_START-3 //!<产生RKI主机认证令牌失败
#define PED_RET_ERR_RKI_SIG_ERR PED_RET_ERR_RKI_START-4		//!<签名失败
#define PED_RET_ERR_RKI_DEV_KEY_ERR PED_RET_ERR_RKI_START-5 //!<密钥错误
#define PED_RET_ERR_RKI_NOT_AUTH	PED_RET_ERR_RKI_START-6 //!<RKI双向未认证
#define PED_RET_ERR_TLV_ERR	PED_RET_ERR_RKI_START-7 //!<RKI 数据TLV错误

#define CFCA_PAXRKI_OU1_LEN 20
///证书中SUBJECT NAME，OU1域，CFCA给PAX分配的唯一编码，用于识别给PAX签发的证书
#define CFCA_PAXRKI_OU1   "PAXTechnology-RKI001"
#define CFCA_O	"China Financial Certification Authority"

#ifdef TEST_RKICA_OCA1
#define CFCA_CN "CFCA TEST OCA1"
#else
#define CFCA_CN "CFCA ACS OCA31"
#endif

#define PEDRKI_DA_PUK_IDX 0
#define PEDRKI_DA_PVK_IDX 0
#define PEDRKI_DA_KEY_IDX 0 //!<设备公钥, 仅适用于MPOS RKI,仅D180支持
#define PEDRKI_VAK_PUK_IDX 1 //!<厂商认证公钥，仅适用于MPOS RKI,仅D180支持
#define PEDRKI_AAK_PUK_IDX 2 //!<收单认证公钥，仅适用于MPOS RKI,仅D180支持
#define PEDRKI_DME_PUK_IDX 3 //!<设备报文加密公钥，仅适用于MPOS RKI,仅D180支持
#define PEDRKI_DEVAUTH_KEY_IDX  4 //!<设备认证公钥索引，适用于用于POS RKI
#define PEDRKI_DEVENC_KEY_IDX 5 //!<设备加密公钥索引，适用于用于POS RKI
#define PEDRKI_RKIAK_PUK_IDX 6 //!<RKI认证公钥索引，适用于用于POS RKI

#define PED_RKI_CERT_EXIST 1 //!<证书已存在
#define PED_RKI_KEY_PAIRS_EXIST 2 //!<密钥对已存在


#define PED_RKI_ENC_ALG_ECB 0X00 //!<加密算法ECB
#define PED_RKI_ENC_ALG_CBC 0X01 //!<加密算法CBC

#define PED_RKI_CODE_ALG_DER 0X00 //!<私钥编码算法DER
///私钥编码算法BIN， 4字节模位数（小端）+模+指数+后补0x00
#define PED_RKI_CODE_ALG_BIN 0X01 
///OWF发散算法
#define PED_RKI_DERIVE_ALG_OWF 0X00

///TLV格式为：1字节TAG+2字节长度（小端，低字节在前）+TAG值。TAG定义如下：
enum
{
	PEDRKI_TAG_DA_PUK= 0x01,	   //!<设备认证公钥
	PEDRKI_TAG_DA_PVK=0x02,		   //!<设备认证私钥
	PEDRKI_TAG_VAK_PUK=0x03,	   //!<厂商认证公钥
	PEDRKI_TAG_VSK_PUK=0x04,       //!<厂商签名公钥
	PEDRKI_TAG_AAK_PUK=0x05,       //!<收单认证公钥 
	PEDRKI_TAG_DME_PUK=0x06,       //!<设备报文加密密钥保护公钥
	PEDRKI_TAG_AT_H=0x07 ,         //!<主机签名令牌
	PEDRKI_TAG_AT_D=0x08,          //!<设备鉴别令牌  
	PEDRKI_TAG_TK=0x09,            //!<临时传输密钥 
	PEDRKI_TAG_TMK=0x0a,           //!<设备主密钥
	PEDRKI_TAG_SHA1=0x0b,          //!</SHA1校验值
	PEDRKI_TAG_SHA256=0x0c,        //!</SHA256校验值 
	PEDRKI_TAG_ENC_ALG=0x0d,       //!<加密方式
	PEDRKI_TAG_CODE_ALG=0x0e,      //!<编码方式
	PEDRKI_TAG_TMEK=0x0f,          //!<报文加密根密文
	PEDRKI_TAG_TMEK_INFO=0x10,     //!<报文加密根密文信息，8字节TMEK的KCV，用于KMS识别TMEK
	PEDRKI_TAG_KCV=0x11,           //!<KCV
	PEDRKI_TAG_DERIVE_VAR=0x12,    //!<密钥发散因子
	PEDRKI_TAG_DERIVE_ALG=0x13,    //!<发散算法
	PEDRKI_TAG_DEV_AUTH_DATA=0x14, //!<设备认证数据
	PEDRKI_TAG_PUK_INFO=0x15,      //!<公钥信息
	PEDRKI_TAG_DME_PUK_MAIL=0x16,  //!<报文加密公钥信封 
	PEDRKI_TAG_KMS_ID=0x17,        //!<KMSID
	PEDRKI_TAG_TEK=0x18,           //!<TEK
	PEDRKI_TAG_AUK=0x19,           //!<AUK 
	PEDRKI_TAG_TEKAUK_CIPHER=0x1a, //!<TEKAUK CIPHER 
	//ADD 20150819
	PEDRKI_TAG_PUK_FMT=0x1b,       //!<公钥格式
	PEDRKI_TAG_DE_PUK=0x1c,        //!<设备加密公钥 
	PEDRKI_TAG_DE_PVK=0x1d,        //!<设备加密私钥
	PEDRKI_TAG_LKIAK_PUK=0X1e,     //!<LKI 认证公钥
	PEDRKI_TAG_RKIAK_PUK=0X1f,     //!<RKI认证公钥
	PEDRKI_TAG_DEV_ID=0X20,        //!<厂商|机型|序列号
	PEDRKI_TAG_CRL=0X21,           //!<吊销列表
	PEDRKI_TAG_KTK=0X22,           //!<传输密钥密文
	PEDRKI_TAG_KEY_CNT=0X23,       //!<密钥个数 
	PEDRKI_TAG_TR31_KEYBLK=0X24,   //!<TR31密钥块
	PEDRKI_TAG_LIST=0X25,          //!<标签列表，value可包含多个标签
	/*	
	PEDRKI_TAG_OPT_PUK=0x26,       //!<操作员公钥
	PEDRKI_TAG_OPT_AUTH_DATA=0x27, //!<操作员认证数据
	PEDRKI_TAG_AT_OPT=0x28,        //!<操作员鉴别令牌  
	*/
	PEDRKI_TAG_INVALID=0xff,       //!<标签非法
};

enum
{
	///RKI状态
	PEDRKI_INFO_STATUS=0X01,
	///RKI KMS ID
	PEDRKI_INFO_KMS_ID=0X02,
};

#define PED_RKI_KMSID_LEN 32
#define PED_RKI_MAX_CERT_LEN 1270

/// #PEDRKI_TAG_PUK_FMT公钥格式-仅公钥结构
#define PUK_FMT_RAW      (uchar)0x00//
/// #PEDRKI_TAG_PUK_FMT公钥格式-PAX 自定义证书
#define PUK_FMT_PAX_CERT (uchar)0x01//
/// #PEDRKI_TAG_PUK_FMT公钥格式-X509证书
#define PUK_FMT_X509     (uchar)0x02
/// #PEDRKI_TAG_PUK_FMT公钥格式-PKCS10自签名证书
#define PUK_FMT_P10		 (uchar)0x03

//TR31-definition 
//key usage
#define PED_KU_DUKPT_INIT_KEY             ( (unsigned short) (('B'<<8)|'1') ) //!< "B1". DUKPT init key.
#define PED_KU_KEY_ENC_OR_WRAP            ( (unsigned short) (('K'<<8)|'0') )//!< "K0". 包含 MK.
//key  Algorithm
#define PED_KA_TDEA                       ( (char)('T') )      //!< Triple DEA
//key mode of use
#define PED_MOU_DERIVE_KEYS          ( (char)('X') )    //!< Key used to derive other key(s) : 可以是 DUKPT init key 的 属性. 
#define PED_MOU_DEC_OR_UNWRAP_ONLY   ( (char)('D') )    //!< Decrypt / Unwrap Only : 只能用来解密, 比如 MK.
#define SEC_MOU_ENC_DEC_WRAP_UNWRAP  ( (char)('B') )    //!< Both Encrypt & Decrypt / Wrap &Unwrap. "KUM" : Key Use Mode.
#define SEC_MOU_DEC_OR_UNWRAP_ONLY   ( (char)('D') )    //!< Decrypt / Unwrap Only : 只能用来解密, 比如 MK.
#define SEC_MOU_ENC_OR_WRAP_ONLY     ( (char)('E') )    //!< Encrypt / Wrap Only : 只能用在加密, 比如 PIN key. 

#define PED_KV_NOT_USED          ( (unsigned short)(('0'<<8)|'0') )//!<密钥无版本
#define PED_KE_NON_EXPORTABLE                 ( (char)('N') )      //!< 不可导出 

/****macrodef of optional BLOCK ID**********************************/
#define PED_OPT_KEY_BLOCK_ID_KSN ((unsigned short)('K'<<8|'S'))//!<‘KS’ 0x4B53 Key Set Identifier,
#define PED_ENC_KEY_FMT_TR31_MAC_LEN 8 //!< TR31- 密文中MAC长度

// structure for ped operation 
// nicm add 2008-07-25 end
//=============================================================================

//----------------------------------------------------------
// nicm add for new wireless module begin
//----------------------------------------------------------
#define WL_CREG_CGREG_READY	4	//WlInitEx返回成功	
#define WL_RET_ERR_PORTINUSE -201 // 模块口被占用
#define WL_RET_ERR_NORSP -202 // 模块没有应答
#define WL_RET_ERR_RSPERR -203 // 模块应答错误
#define WL_RET_ERR_PORTNOOPEN -204 // 模块串口没有打开
#define WL_RET_ERR_NEEDPIN -205	// 需要PIN码
#define WL_RET_ERR_NEEDPUK -206	// 需要PUK解PIN码
#define WL_RET_ERR_PARAMER -207	// 参数错误
#define WL_RET_ERR_ERRPIN -208 // 密码错误
#define WL_RET_ERR_NOSIM -209 // 没有插入SIM卡
#define WL_RET_ERR_NOTYPE -210 // 不支持的类型
#define WL_RET_ERR_NOREG -211 // 网络没有注册
#define WL_RET_ERR_INIT_ONCE -212 // 模块已初始化
#define WL_RET_ERR_LINEOFF -214	// 连接断开
#define WL_RET_ERR_TIMEOUT -216	// 超时
#define WL_RET_ERR_REGING -222 // 网络注册中
#define WL_RET_ERR_PORTCLOSE -223 // 关闭串口出错
#define WL_RET_ERR_MODEVER -225	// 错误的模块版本
#define WL_RET_ERR_DIALING -226	// 拨号中  
#define WL_RET_ERR_ONHOOK -227 // 关机中
#define WL_RET_ERR_PPP_BRK -228	// 发现PPP断开
#define WL_RET_ERR_NOSIG -229 // 网络无信号
#define WL_RET_ERR_POWEROFF -230 // 模块已下电
#define WL_RET_ERR_BUSY -231 // 模块忙
#define WL_RET_ERR_OPENPORT -232 // 不能打开串口
#define WL_RET_ERR_PPPONLINE  -234    //PPP已在线
#define WL_RET_ERR_OTHER -300 // 其他未知错误

#define	NET_OK               0  // 无错误，正常
#define	NET_ERR_MEM     	 -1  // 内存不够
#define	NET_ERR_BUF     	 -2  // 缓冲区错误
#define	NET_ERR_ABRT    	 -3  // 试图建立连接失败
#define	NET_ERR_RST     	 -4  // 连接被对方复位（收到对方的Reset）
#define	NET_ERR_CLSD    	 -5  // 连接已关闭
#define	NET_ERR_CONN    	 -6  // 连接未成功建立
#define	NET_ERR_VAL     	 -7  // 错误变量
#define	NET_ERR_ARG     	 -8  // 错误参数
#define	NET_ERR_RTE     	 -9  // 错误路由(route)
#define	NET_ERR_USE     	 -10 // 地址、端口使用中
#define	NET_ERR_IF      	 -11 // 底层硬件错误
#define	NET_ERR_ISCONN  	 -12 // 连接已建立
#define	NET_ERR_TIMEOUT 	 -13 // 超时
#define	NET_ERR_AGAIN   	 -14 // 请求资源不存在，请重试
#define	NET_ERR_EXIST   	 -15 // 已存在
#define	NET_ERR_SYS     	 -16 // 系统不支持
#define	NET_ERR_PASSWD  	 -17 // 错误密码
#define	NET_ERR_MODEM   	 -18 // 拨号失败
#define	NET_ERR_LINKDOWN   	 -19 // 数据链路已断开，请重新拨号
#define	NET_ERR_LOGOUT	   -20 // Logout
#define	NET_ERR_PPP	       -21 // PPP断开
#define NET_ERR_STR        -22 // String Too Long, Illeage
#define NET_ERR_DNS        -23 // DNS Failure: No such Name
#define NET_ERR_INIT       -24 // No Init
#define NET_ERR_NEED_DHCP  -25 //DHCP未开启
#define NET_ERR_LINK       -26 //链路错误
#define NET_ERR_SERV	   -30 //没有找到PPPoE服务器
#define NET_ERR_IRDA_SYN   -51 //座机重启或放置在不同的座机上（S60，D210专用）
#define NET_ERR_IRDA_COMM  -54 //座机和手机之间通信失败



//----------------------------------------------------------
// nicm add for new wireless module end
//----------------------------------------------------------

//=============================================
//     Asynchronism communication functions
//=============================================
#define COM1        	0
//#define COM2            2     P90
#define COM2        	1
#define COM_USB			11
#define RS485       	2
#define PINPAD      	3
#define IC_COMMPORT		4

#define BASECOM1    	0
#define BASECOM2    	1
#define BASERS485   	2
#define BASEPINPAD  	3
#define HANDSETCOM1 	4

#define RS232A			0
#define RS232B			1
#define LANPORT 		2
#define MODEM			4
#define WNETPORT        1

//==================================================
//     Macros for encription/decription fucntions
//==================================================
#define ENCRYPT 1
#define DECRYPT 0

//===========================================
//        Defined for file system
//============================================
#define	FILE_EXIST		1
#define	FILE_NOEXIST	2
#define	MEM_OVERFLOW	3
#define	TOO_MANY_FILES	4
#define	INVALID_HANDLE	5
#define	INVALID_MODE	6
#define NO_FILESYS		7
#define FILE_NOT_OPENED	8
#define	FILE_OPENED		9
#define END_OVERFLOW	10
#define TOP_OVERFLOW	11
#define NO_PERMISSION	12
#define FS_CORRUPT		13

#define	O_RDWR		0x01
#define	O_CREATE	0x02
#define	O_ENCRYPT   0x04

typedef struct
{
	unsigned char	fid;
	unsigned char	attr;
	unsigned char	type;
	char			name[17];
	unsigned long	length;
} FILE_INFO;

//extern int errno;
extern int _app_para;


typedef struct
{
	 /*发散该密钥的源密钥的密钥类型，
	 PED_TLK,PED_TMK,PED_TPK,PED_TAK,PED_TDK, PED_TAESK, 
	 不得低于ucDstKeyType所在的密钥级别*/
	 uchar ucSrcKeyType;
	 /* 发散该密钥的源密钥索引，
	 索引一般从1开始，如果该变量为0，
	 则表示这个密钥的写入是明文形式*/
	 uchar ucSrcKeyIdx;
	 /* 目的密钥的密钥类型: PED_TAESK */
	 uchar ucDstKeyType;
	 /*目的密钥索引*/
	 uchar ucDstKeyIdx;
	 /*目的密钥长度，16,24,32 */
	 int iDstKeyLen;
	 /*写入密钥的内容*/
	 uchar aucDstKeyValue[32];
}ST_AES_KEY_INFO;


//========================================================
//     MultiApplication functions
//========================================================
typedef struct {
unsigned char AppName[32];
unsigned char AID[16];
unsigned char AppVer[16];
unsigned char AppProvider[32];
unsigned char Descript[64];
unsigned char DownloadTime[14];
unsigned long MainAddress;
unsigned long EventAddress;
unsigned char AppNum;
unsigned char RFU[73];
}APPINFO;


#define MAGCARD_MSG   0x01
#define ICCARD_MSG    0x02
#define KEYBOARD_MSG  0x03
#define USER_MSG      0x04

typedef struct{
	unsigned char RetCode;
	unsigned char track1[256];
	unsigned char track2[256];
	unsigned char track3[256];
}ST_MAGCARD;

typedef struct{
	int MsgType;		    //MAGCARD_MSG,ICCARD_MSG,KEYBOARD_MSG,USER_MSG
	ST_MAGCARD MagMsg;      //MAGCARD_MSG
	unsigned char KeyValue;  //ICCARD_MSG
	unsigned char IccSlot;   //KEYBOARD_MSG
	void *UserMsg;           //USER_MSG
}ST_EVENT_MSG;

//=================================================
//			  for RF card functions
//=================================================
typedef struct
{
	unsigned char  drv_ver[5];  //驱动程序的版本信息，如：”1.01A”；只能读取，写入无效
	unsigned char drv_date[12];  // 驱动程序的日期信息，如：”2006.08.25”； 只能读取
	unsigned char a_conduct_w;  //A型卡输出电导写入允许：1--允许，其它值—不允许
	unsigned char a_conduct_val;  // A型卡输出电导控制变量，有效范围0~63,超出时视为63
	//a_conduct_w=1时才有效，否则该值在驱动内部不作改变//用于调节驱动A型卡的输出功率，由此能调节其最大感应距离
	unsigned char m_conduct_w;  //M1卡输出电导写入允许：1--允许，其它值—不允许
	unsigned char m_conduct_val;  // M1卡输出电导控制变量，有效范围0~63,超出时视为63
	//m_conduct_w=1时才有效，否则该值在驱动内部不作改变 //用于调节驱动M1卡的输出功率，由此能调节其最大感应距离
	unsigned char b_modulate_w;  // B型卡调制指数写入允许：1--允许，其它值—不允许
	unsigned char b_modulate_val;  // B型卡调制指数控制变量，有效范围0~63,超出时视为63
	// b_modulate_w=1时才有效，否则该值在驱动内部不作改变,用于调节驱动B型卡的调制指数，由此能调节其最大感应距离
	unsigned char  card_buffer_w;  //卡片接收缓冲区大小写入允许：1--允许，其它值—不允许
	unsigned short card_buffer_val; //卡片接收缓冲区大小参数（单位：字节），有效值1~256。
	//大于256时，将以256写入；设为0时，将不会写入。
	//卡片接收缓冲区大小直接决定了终端向卡片发送一个命令串时是否需启动分包发送、以及各个分包的最大包大小。若待发送的命令串大于卡片接收缓冲区大小，则需将它切割成小包后，连续逐次发送
	//在PiccDetect( )函数执行过程中，卡片接收缓冲区大小之参数由卡片报告给终端，一般无需更改此值。但对于非标准卡，可能需要重设此参数值，以保证传输有效进行
	unsigned char  wait_retry_limit_w;// S(WTX)响应发送次数写入允许：1--允许，其它值—不允许 (暂不适用)
	unsigned short wait_retry_limit_val;//S(WTX)响应最大重试次数, 默认值为3(暂不适用)
	// 20080617 
	unsigned char card_type_check_w; // 卡片类型检查写入允许：1--允许，其它值--不允许，主要用于避免因卡片不规范引起的问题
	unsigned char card_type_check_val; // 0-检查卡片类型，其他－不检查卡片类型(默认为检查卡片类型)
	//2009-10-30
	unsigned char card_RxThreshold_w; //接收灵敏度检查写入允许：1--允许，其它值--不允许，主要用于避免因卡片不规范引起的问题
	unsigned char card_RxThreshold_val;//天线参数为5个字节时，为A卡和B卡的接收灵敏度，天线参数为7个字节时为B卡接收灵敏度
	//2009-11-20
	unsigned char f_modulate_w; // felica调制指数写入允许
	unsigned char f_modulate_val;//felica调制指数

	//add by wls 2011.05.17
	unsigned char a_modulate_w; // A卡调制指数写入允许：1--允许，其它值—不允许
	unsigned char a_modulate_val; // A卡调制指数控制变量，有效范围0~63,超出时视为63
	
    //add by wls 2011.05.17
	unsigned char a_card_RxThreshold_w; //接收灵敏度检查写入允许：1--允许，其它值--不允许
	unsigned char a_card_RxThreshold_val;//A卡接收灵敏度
	
	//add by liubo 2011.10.25, 针对A,B和C卡的天线增益
	unsigned char a_card_antenna_gain_w;
	unsigned char a_card_antenna_gain_val;
	
	unsigned char b_card_antenna_gain_w;
	unsigned char b_card_antenna_gain_val;
	
	unsigned char f_card_antenna_gain_w;
	unsigned char f_card_antenna_gain_val;
	
	//added by liubo 2011.10.25，针对Felica的接收灵敏度
	unsigned char f_card_RxThreshold_w;
	unsigned char f_card_RxThreshold_val;

	/* add by wanls 2012.08.14*/
	unsigned char f_conduct_w;  
	unsigned char f_conduct_val; 
	
	/* add by nt for paypass 3.0 test 2013/03/11 */
	unsigned char user_control_w;
	unsigned char user_control_key_val;

	/* add by wanls 20141106*/
	unsigned char protocol_layer_fwt_set_w;
	unsigned int  protocol_layer_fwt_set_val;
	/* add end */

	/* add by wanls 20150921*/
	unsigned char picc_cmd_exchange_set_w;
	/*Bit0 = 0 Disable TX CRC, Bit0 = 1 Enable TX CRC*/
	/*Bit1 = 0 Disable RX CRC, Bit1 = 1 Enable RX CRC*/
	unsigned char picc_cmd_exchange_set_val;
	/* add end */
	/*add by zhaoyj  20170419*/
	unsigned char a_card_filter_w;
	unsigned char a_card_filter_val;

	unsigned char b_card_filter_w;
	unsigned char b_card_filter_val;

	unsigned char f_card_filter_w;
	unsigned char f_card_filter_val;
	/*add end*/
	unsigned char reserved[54]; //modify by nt 2013/03/11 original 74.保留字节，用于将来扩展；写入时应全清零
//	unsigned char reserved[60]; //modify by nt 2013/03/11 original 74.保留字节，用于将来扩展；写入时应全清零


}PICC_PARA;/*size of PICC_PARA is 124 Bytes*/
#define RET_RF_ERR_USER_CANCEL         0x27 /* add by nt for paypass 3.0 test 2013/03/11 */


//-----------------------------------------------------------------
// nicm add for R50 射频卡模块增加 Desfire 功能支持 2008-09-23 begin
//-----------------------------------------------------------------
//  Structure for returning the information supplied by the GetVersion command.
typedef struct
{
    uchar    ucHwVendorID;
    uchar    ucHwType;
    uchar    ucHwSubType;
    uchar    ucHwMajorVersion;
    uchar    ucHwMinorVersion;
    uchar    ucHwStorageSize;
    uchar    ucHwProtocol;

    uchar    ucSwVendorID;
    uchar    ucSwType;
    uchar    ucSwSubType;
    uchar    ucSwMajorVersion;
    uchar    ucSwMinorVersion;
    uchar    ucSwStorageSize;
    uchar    ucSwProtocol;

    uchar    aucUid[ 7 ];
    uchar    aucBatchNo[ 5 ];
    uchar    ucProductionCW;
    uchar    ucProductionYear;
}VERSION_INFO;

typedef struct
{
//	uchar ucFileNo;
	uchar ucCommSetting;
	uchar aucAccRight[2];
	uchar aucFileSize[3]; 
}STR_DATAFILE;

typedef struct
{
//	uchar ucFileNo;
	uchar ucCommSetting;
	uchar aucAccRight[2];
	uchar aucLowerLmt[4]; // 有符号整数, 可以为负数
	uchar aucUpperLmt[4]; // 有符号整数, 可以为负数
	uchar aucValue[4]; // 有符号整数, 可以为负数
	uchar ucLmtCrdtEnable;
}STR_VALUEFILE;

typedef struct
{
//	uchar ucFileNo;
	uchar ucCommSetting;
	uchar aucAccRight[2];
	uchar aucRecSize[3];  
	uchar aucMaxRecNum[3];
	uchar aucCurRecNUm[3];
}STR_RECORDFILE;

typedef struct 
{
	uchar ucFileNo; // file ID
	uchar aucOffset[3]; 
	uchar aucNumToRead[3]; // data num or record num
	uchar aucSize[3]; // 仅用于Read Record, 即已知文件记录的大小, 若未知, 则置为0.
}STR_READ_DATA_RECORD;

typedef struct {
	int iModulusLen;// the length of modulus bits.
	uchar aucModulus[512]; //Modulus, if the  length of the Modulus is less than 512bytes, It is padded with 0x00 on the left.
	int iExponentLen; //the length of exponent bits 
	uchar aucExponent[512];// Modulus, if the  length of the Exponent is less than 512bytes, It is padded with 0x00 on the left.
	uchar aucKeyInfo[128];// RSA key info, defined by application.
}ST_RSA_KEY;


//-----------------------------------------
// DESFIRE CARD status and error codes
// 即接口中*pucStatus中回送的数据
//-----------------------------------------
#define STAT_DESFIRE_NO_CHANGES        0x0c 
// Insufficient NV-Memory to complete command
#define STAT_DESFIRE_E2PROM_OVERFL     0x0E
// Command code not supported
#define STAT_DESFIRE_CMD_ERR           0x1C
// CRC/MACdoes not match data, Padding bytes not valid
#define STAT_DESFIRE_INTEG_ERR         0x1E
// Invalid key number specified
#define STAT_DESFIRE_NO_SUCH_KEY       0x40
// length of command string invalid
#define STAT_DESFIRE_LENGTH_ERR        0x7E
// current config / status does not allow the requested command
#define STAT_DESFIRE_PERMISSION_DENIED 0x9D
// value of the param invalid
#define STAT_DESFIRE_PARAM_ERR         0x9E
// requested AID not present on PICC
#define STAT_DESFIRE_APP_NOT_FOUND     0xA0
// unrecoverable error within application, application will be disabled(仅在安全命令中返回)
#define STAT_DESFIRE_APP_INTEG_ERR     0xA1
// current authentication status does not allow the requested command
#define STAT_DESFIRE_AUTH_ERR          0xAE
// additional data frame is expected to be sent
#define STAT_DESFIRE_ADDITIONAL_FRAME  0xAF
// attempt to read/write data from/to beyond the file's/record's limits.
// attempt to exceed the limits of a value file
#define STAT_DESFIRE_BOUNDARY_ERR      0xBE
// unrecoverable error within PICC, PICC will be disabled(仅用于安全命令中)
#define STAT_DESFIRE_PICC_INTEG_ERR    0xC1
// previous command was not fully completed
// not all frames were requested or provided by the PCD
#define STAT_DESFIRE_CMD_ABORTED       0xCA
// PICC was disabled by an unrecoverable error(仅在安全命令中返回)
#define STAT_DESFIRE_PICC_DISABLED_ERR 0xCD
// number of app limited to 28, no additional CreateApplication possible
#define STAT_DESFIRE_COUNT_ERR         0xCE
// Creation of file/app failed because file/app with same number alredy exists
#define STAT_DESFIRE_DUPLICATE_ERR     0xDE
// could not complete NV-write operation due to loss of power, 
// internal backup/rollback mechanism activated(仅在安全命令中返回)
#define STAT_DESFIRE_E2PROM_ERR        0xEE
// specified file number does not exist
#define STAT_DESFIRE_FILE_NOT_FOUND    0xF0
// unrecoverable error within file, file will be disabled(仅在安全命令中返回)
#define STAT_DESFIRE_FILE_INTEG_ERR    0xF1

#define MAX_RSA_MODULUS_BITS 2048

#define MAX_RSA_MODULUS_LEN ((MAX_RSA_MODULUS_BITS + 7) / 8)
#define MAX_RSA_PRIME_BITS ((MAX_RSA_MODULUS_BITS + 1) / 2)
#define MAX_RSA_PRIME_LEN ((MAX_RSA_PRIME_BITS + 7) / 8)

//-----------------------------------------------------------------
// nicm add for R50 射频卡模块增加 Desfire 功能支持 2008-09-23 end
//-----------------------------------------------------------------


typedef struct
{
	unsigned char auth_id;			//论证码
	unsigned char identify_code;	// 识别码
	unsigned char masterkey_id;		// 主密钥索引
	unsigned char pinkey_id;		// PIN密钥索引
	unsigned char key_mode;			// 密钥模式
	unsigned char pin_key[24];		// 密钥
	unsigned char process_mode;		// 加解密模式
} PIN_KEY;

typedef struct
{
	unsigned char auth_id;			// 论证码
	unsigned char identify_code;	// 识别码
	unsigned char masterkey_id;		// 主密钥索引
	unsigned char mackey_id;		// MAC密钥索引
	unsigned char key_mode;			// 密钥模式
	unsigned char mac_key[24];		// 密钥
	unsigned char process_mode;		// 加解密模式
} MAC_KEY;


typedef struct
{
	unsigned char auth_id;			//论证码
	unsigned char identify_code;	//识别码
	unsigned char masterkey_id;		// 主密钥索引
	unsigned char key_mode;			// 密钥模式
	unsigned char master_key[24];	// 密钥
} MASTER_KEY;

typedef struct
{
     unsigned char auth_id;			// 论证码
     unsigned char identify_code;	// 识别码
     unsigned char MKeyID;			// 主密钥索引
     unsigned char WKeyID1;			// PIN或MAC密钥索引
     unsigned char WKeyID2;			// PIN或MAC密钥索引
     unsigned char mode;			// 密钥模式
	 unsigned char keytype;			// 密钥类型
}DERIVE_KEY;


typedef struct
{
	unsigned char auth_id;			// 论证码
	unsigned char identify_code;	// 识别码
	unsigned char input_mode;		// 输入方式
	unsigned char key_id;			// PIN密钥索引
	unsigned char encrypt_mode;		// 加密模式
	unsigned char min_len;			// PIN最小长度
	unsigned char max_len;			// PIN最大长度
	unsigned char card_no[16];		// 卡号

} PIN_ANSIX98;

typedef struct
{
	unsigned char auth_id;			// 论证码
	unsigned char identify_code;	// 识别码
	unsigned char input_mode;		// 输入方式
	unsigned char key_id;			// PIN密钥索引
	unsigned char encrypt_mode;		// 加密模式
	unsigned char min_len;			// PIN最小长度
	unsigned char max_len;			// PIN最大长度
	unsigned char ISN[6];			// ISN

} PIN_ANSIX392;

typedef struct
{
     unsigned char auth_id;			// 论证码
     unsigned char identify_code;	// 识别码
     unsigned char mackeyid;		// MAC密钥索引
     unsigned char mode;			// 加密算法
     unsigned short inLen;			// MAC运算的数据包的长度
     unsigned char datain[2048];	// 需进行MAC运算的数据包
     unsigned char flag;			// MAC算法
} MAC_PACKAGE;

// donot support anymore.
// typedef struct
// {
//      unsigned int  modlen;           //PIN加密公钥模数长
//      unsigned char mod[256];        //PIN加密公钥模数
//      unsigned int  explen;           //PIN加密公钥指数长
//      unsigned char exp[4];          //PIN加密公钥指数
//      unsigned char iccrandomlen;    //从卡行取得的随机数长
//      unsigned char iccrandom[8];    //从卡行取得的随机数
//      unsigned int  termrandomlen;   //从终端应用取得的填充数长
//      unsigned char termrandom[256]; //从终端应用取得的填充数
// } RSA_PINKEY;

// 通讯参数设置
typedef struct
{
    unsigned char bCommMode;        // 0:Serial, 1:Modem, 2:LAN, 3:GPRS, 4:CDMA, 5:PPP
                           // 目前远程下载只支持串口、modem、LAN和PPP四种通讯模式，
                           // 而不支持GPRS和CDMA两种通讯模式  2005-12-22
                            
    unsigned char *psAppName;       // 应用名，若为空则下载所有的应用及参数、数据文件
    unsigned char bLoadType;        // 下载类型，bit0:应用，bit1:参数文件，bit2:数据文件
    unsigned char psProtocol;       // 协议信息：0-protims；1-ftims；2-tims
    unsigned char bPedMode;         // 0-外置PED（如PP20）　1－内置PED（P60－S1，P80）
                           // 目前该参数在ProTims和Tims中不使用，只在FTims中使用
                            
    unsigned char bDMK_index;       // 主密钥索引，ProTims中不使用
    unsigned char sTransCode[5];    // 交易码，只在FTims在中使用，ProTims和Tims中不使用
                           // 取值     含义
                           // "2001"：单应用下载交易
                           // "2011"：多应用下载交易
                           // "1000"：单应用POS查询下载任务
                           // "1005"：单应用POS查询和下载在一次通讯内完成
                           // "1010"：多应用POS查询下载任务
                           // "1015"：多应用POS查询和下载在一次通讯内完成
                            
    unsigned char ucCallMode;    // 调用模式，目前该参数在国内版ProTims、FTims和Tims中
                        // 不使用 2005-09-26
    // 在新版海外版ProTims中使用该参数，主要是香港那边开始使用P80 2005-10-17
    // 应用调用本接口的控制。bit0~bit3: 0：操作员主动要求；
    // 1：按计划定时；
    // 2：被远程服务器呼叫，请求monitor接收server的更新通知
    // 消息(接口返回时，monitor不能断线)；
    // 3：(v0.8.0)被远程服务器呼叫，请求monitor做远程下载操作，
    // 如果bit4~bit7=0, P70的monitor需要用内部串口操作
    // modem。
    // bit4~bit7: 0：monitor不需要建立通讯连接；1：monitor
    // 自己建立连接
    
    unsigned char *psTermID;     // 终端号，目前该参数在海外版本PtoTims和新版本的
                        // 国内ProTims中使用 2006-04-18
    union
    {
        struct
        {
            unsigned char *psPara; //串口通讯参数，格式如"57600,8,n,1"
        }tSerial;  // 串口通讯参数，参数bCommMode=0时该参数才有效
        struct
        {
            unsigned char *psTelNo;         //电话号码，详细参考ModemDial()函数
            COMM_PARA *ptModPara;  //Modem通讯参数
            unsigned char bTimeout;         //拨号成功后的延时[秒]
        }tModem;  // modem通讯参数，参数bCommMode=1时该参数才有效
        struct
        {
            unsigned char *psLocal_IP_Addr;       // 本地IP地址，在TCP/IP方式下必须使用
            unsigned short wLocalPortNo;           // 本地端口号，现在不使用，IP80模块会自动使用默认端口号
            unsigned char *psRemote_IP_Addr;      // 远程IP地址，在TCP/IP方式下必须使用
            unsigned short wRemotePortNo;          // 远程端口号，在TCP/IP方式下必须使用
            unsigned char *psSubnetMask;          //   子网掩码，在TCP/IP方式下必须使用  2006-04-21
            unsigned char *psGatewayAddr;         //       网关，在TCP/IP方式下必须使用      2006-04-21
        }tLAN; // TCP/IP协议通讯参数，参数bCommMode=2时该参数才有效
        struct
        {
            unsigned char *psAPN;          //APN
            unsigned char *psUserName;     //用户名
            unsigned char *psPasswd;       //用户密码
            unsigned char *psIP_Addr;      //IP地址
            unsigned short nPortNo;         //端口号
            unsigned char *psPIN_CODE;     //如果SIM卡有密码，还要输入PIN码
            unsigned char *psAT_CMD;       //如果有AT命令的话
        }tGPRS; // GSM手机通讯参数，参数bCommMode=3时该参数才有效
        struct
        {
            unsigned char *psTelNo;        //电话号码
            unsigned char *psUserName;     //用户名
            unsigned char *psPasswd;       //用户密码
            unsigned char *psIP_Addr;      //IP地址
            unsigned short nPortNo;         //端口号
            unsigned char *psPIN_CODE;     //如果SIM卡有密码，还要输入PIN码
            unsigned char *psAT_CMD;       //如果有AT命令的话
        }tCDMA; // CDMA手机通讯参数，参数bCommMode=4时该参数才有效
        struct 
        {
            unsigned char *psTelNo;        // 电话号码
            COMM_PARA *ptModPara; // MODEM通信参数
            unsigned char *psUserName;     // 用户名
            unsigned char *psPasswd;       // 用户密码
            unsigned char *psIP_Addr;      // IP地址
            unsigned short nPortNo;         // 端口号
        }tPPP; // 基于modem的点对点(Point to Point Protocol)远程下载通讯参数，
               // 参数bCommMode=5时该参数才有效

		struct //WIFI 参数bCommMode=6时该参数才有效
		{
			
			unsigned char *Wifi_SSID;        // SSID
			unsigned char *psPasswd;       // 用户密码
			
			unsigned char *Local_IP; // 本机IP
			
			unsigned short Local_PortNo;
			
			unsigned char *Remote_IP_Addr;      // 远程IP地址
			unsigned short RemotePortNo;          // 远程端口号
			unsigned char *SubnetMask;          //   子网掩码
			unsigned char *GatewayAddr;         //       网关
			unsigned char *Dns1; 
			unsigned char *Dns2; 	
			unsigned char Encryption_Mode; //  1: WEP 64(ASCII); 2 WEP 128 (ASCII);    3  WPA_TKIP;  4 WPA_AES  	
			unsigned char Encryption_Index; // WEP时用取值范围:1~4
			unsigned char DHCP_Flag; // 1:使用DHCP 
        }tWIFI; 
    }tUnion;
}T_INCOMMPARA;

typedef struct
{
    uchar app_name[33];    // 应用名称
    uchar app_ver[21];     // 应用版本
    uchar have_param;      // 有无参数文件
    uchar param_name[17];  // 参数文件名
    uchar param_ver[21];   // 参数文件版本
    uchar UpdatedAppMode;  // 0-立即更新 1-保存在临时文件中
    uchar UpdatedParaMode; // 0-立即更新 1-保存在临时文件中
    uchar AppActiveStatus; // 0-未激活　 1-已激活
    uchar reserve[4];      // 保留字节    
}LOADLOG_INFO; // 100byte

typedef struct
{
	uchar bAppTotal;           // POS机中的应用总数
	uchar sAppNum[25];        // 每个应用的序号
	uchar sLoadAppStat[25];  // 对应的每个应用的下载状态
	uchar sLoadParaStat[25];  // 对应的每个应用中参数/数据文件的下载状态
	uchar sAppName[25][33];   // 应用名称汇总
}T_LOADSTATUS;

typedef struct {
	unsigned short int bits;                     /* length in bits of modulus */
	unsigned char modulus[MAX_RSA_MODULUS_LEN];  /* modulus */
	unsigned char exponent[MAX_RSA_MODULUS_LEN]; /* public exponent */
}R_RSA_PUBLIC_KEY;

typedef struct {
	unsigned short int bits;								/* length in bits of modulus */
	unsigned char modulus[MAX_RSA_MODULUS_LEN];				/* modulus */
	unsigned char publicExponent[MAX_RSA_MODULUS_LEN];		/* public exponent */  
	unsigned char exponent[MAX_RSA_MODULUS_LEN];			/* private exponent */
	unsigned char prime[2][MAX_RSA_PRIME_LEN];				/* prime factors */
	unsigned char primeExponent[2][MAX_RSA_PRIME_LEN];		/* exponents for CRT */
	unsigned char coefficient[MAX_RSA_PRIME_LEN];			/* CRT coefficient */
}R_RSA_PRIVATE_KEY;

enum CHANNEL_NO
{
	P_RS232A=0,
	P_RS232B,
	P_WNET,
	P_PINPAD,
	P_MODEM,
	P_WIFI,
	CHANNEL_LIMIT,
    P_USB=10,
    P_USB_DEV,
    P_USB_HOST
};


/**********************************************/
/*                                            */
/*　　　　　函数返回值一览表　　              */
/*                                            */
/**********************************************/
/*
   0x00	  正确
   0x01	  数据包长度非法
   0x02	  密钥索引非法
   0x03	  非法模式
   0x04	  屏幕坐标非法
   0x05	  字符串可输入长度范围非法
   0x06	  操作员取消输入
   0x07	  输入超时
   0x08	  显示点阵长度非法
   0x09	  两次密码不一致
   0x0a	  用户未输入密码（输入密码的长度为0）
   0x0b	  指定的密钥不存在
   0x0c   内存错
   0x0d	  奇校验错
   0x0e	  TMK错
   0x0f	  "输入MAC"错
   0x10	  菜单序号非法
   0x11	  密钥检验不合法

   0x12   应用外部论证密钥已初始化
   0x13   应用数目溢出
   0x14   应用不存在
   0x15   外部论证失败
   0x16   没有进行外部认证
   0x17   PIN专用密钥太多
   0x18   无应用名称(或应用名称超长）
   0x19   外部论证锁定（连续尝试失败超过15次）
   0x1a   输入间隔小于30秒

   0xc6	  用户取消输入
   0xce	  PED已锁
   0xcf	  对PED解锁失败

*/



//Module status related macros
#define WNET_UNKNOWN	0x00
#define WNET_CDMA		0x01
#define WNET_GPRS		0x02
//#define WNET_DETACHED	0x00
//#define WNET_ATTACHED	0x01
//Port related macros
#define COMNUM			0x02	//Serial port number, CPU to Module
//#define COMNUM              0x01    //Serial port number, CPU to Module P90

#define PORT_CLOSED		0x00
#define PORT_OPENED		0x01

#define PSEND_OK		0x00	//serial port send successfully
#define PSEND_ERR		0x01	//serial port send error
#define PRECV_UNOPEN	-2
#define PRECV_ERR		-1		//serial port receive error
#define PRECV_TIMEOUT	0		//serial port receive timeout

//General macros
#define RET_OK			0x00	//return successfully
#define RET_ERR			0x01	//error occurred
#define RET_NORSP		0x02	//no response from the module
#define RET_RSPERR		0x03	//"ERROR" is return from the module
#define RET_NOSIM		0x04	//SIM/UIM card is not inserted or not detected
#define RET_NEEDPIN		0x05	//SIM PIN is required
#define RET_NEEDPUK		0x06	//SIM PUK is required
#define RET_SIMBLOCKED	0x07	//SIM Card is permanently blocked
#define RET_SIMERR		0x08	//SIM card does not exist or needs SIM PIN
#define RET_PINERR		0x09	//SIM PIN is incorrect
#define RET_NOTDIALING	0x0A	//the module is not in dialing status
#define RET_PARAMERR	0x0B	//parameter error
#define RET_FORMATERR	0x0C	//Format error
#define RET_SNLTOOWEAK	0x0D	//the signal is too weak, please check the antenna
#define RET_LINKCLOSED	0x0E	//the module is offline
#define RET_LINKOPENED	0x0F	//the module is online
#define RET_LINKOPENING	0x10	//the module is connecting to the network
#define RET_TCPCLOSED	0x11	//tcp socket is closed
#define RET_TCPOPENED	0x12	//tcp socket is opened
#define RET_TCPOPENING	0x13	//the module is trying to open a TCP socket
#define RET_ATTACHED	0x14	//Attached
#define RET_DETTACHED	0x15	//Dettached
#define RET_ATTACHING	0x16	//the module is looking for the base station.
#define RET_NOBOARD		0x17	//no GPRS or CDMA board exist
#define RET_UNKNOWNTYPE     0x18    //unknown type
#define RET_EMERGENCY       0x19    //SIM/UIM is in emergency status

#define RET_RING            0x1A    //  detect ringing
#define RET_BUSY            0x1B    //  detect call in progress
#define RET_DIALING         0x1C    //  dialing

#define RET_PORTERR		0xFD	//serial port error
#define RET_PORTINUSE	0xFE	//serial port is in use by another program
#define RET_ABNORMAL	0xFF	//abnormal error


/**********************************************
            Functions Declaration
**********************************************/

typedef struct
{
	uchar connected;
	uchar remote_closed;
	uchar has_data_in;
	uchar outbuf_emptied;
	ushort left_outbuf_count;
	uchar remote_addr[4];
	uchar reserved1[30];
	long link_state;
	uchar local_addr[4];
	uchar dns1_addr[4];
	uchar dns2_addr[4];
	uchar gateway_addr[4];
	uchar subnet_mask[4];
	uchar reserved2[30];
}PPP_NET_INFO;


//================================================
//	     PPP connection functions
//================================================
typedef struct
{
  uchar IsSync;//0--async connect,1--sync connect
  uchar ConnectTimeout;//connect timeout for a single no.,0--default to 30s
  uchar SpeedType;//0-default to highest of the GSM chip,for g20 async is 9600
							//1-1200,2-2400,3-4800,4-7200,5-9600,6-12000,7-14400
							//8-19.2k,9-24k,10-26.4k,11-28.8k,12-31.2k,13-33.6k
							//14-48k,15-56k
  uchar AsyncFormat;//0-N81,1-E81,2-O81,3-N71,4-E71,5-O71
  uchar IdleTimeout;//unit:10s,0--no auto hangup
  uchar DialTimes;//dial times for a single number,0--default to 1
  uchar DisableXonXoff;//0-enable XonXoff software flow control,1-disable
  uchar ServiceType;//0-PPP,1-normal ASYNC,other--reserved
  uchar Reserved[50];//must all filled with 0
}GSM_CALL;

//---------------------------
// 2009-08-10 add ssl api
//---------------------------

typedef struct 
{  
    void * ptr;/* 内存指针*/
	int  size;/* 空间大小*/
} ST_SSL_BUF;

#define SSL_OPER_OK			0
#define SSL_BEYOND_CERF_NUM	-401//证书个数过多
#define SSL_BEYOND_SERV_IP	-404//太多服务端，多于10个
#define SSL_FILE_OPER_FAIL	-405//文件操作失败,
#define SSL_CER_NO_EXIST	-406//证书不存在
#define SSL_CER_NOSPACE		-407//证书空间不够
#define SSL_NULL_ERR		-408//参数为非法空串


//================================================
//            Printer functions
//================================================
#define     PRN_OK              0x00
#define     PRN_BUSY            0x01
#define     PRN_PAPEROUT        0x02
#define     PRN_WRONG_PACKAGE   0x03
#define     PRN_FAULT           0x04
#define     PRN_TOOHEAT         0x08
#define     PRN_UNFINISHED      0xf0
#define     PRN_NOFONTLIB       0xfc
#define     PRN_OUTOFMEMORY     0xfe

//================================================
//            Bluetooth functions
//================================================
//BT functions
#define STRING_LEN 64

#define COM_BT	7
//MINI
#define BT_ROLE_MASTER	1
#define BT_ROLE_SLAVER	0
#define BT_STATE_NOTCONNECT 0
#define BT_STATE_CONNECT 1
typedef enum{ 
	eBtCmdGetMode = 0, /* 获取模式 */ 
	eBtCmdSetMode, /* 设置模式*/ 
	eBtCmdGetConfig, /* 读配置 */ 
	eBtCmdSetConfig, /* 写配置 */ 
	eBtCmdReset, /* 复位蓝牙模块 */ 
	eBtCmdDropLink, /*断开连接*/ 
	eBtCmdGetLinkStatus, /* 取连接状态 */ 
	eBtCmdDisableBroadcast, /* 关闭蓝牙广播for Brazil*/ 
	eBtCmdEnableBroadcast, /* 打开蓝牙广播for Brazil */ 
	eBtCmdDisablePIN, /* 关闭蓝牙PIN验证功能*/ 
	eBtCmdEnablePIN, /* 打开蓝牙PIN验证功能*/
	eBtCmdEnablePasskey = 13, /* 打开PasskeyEntry配对功能（拉卡拉版本不支持）*/ 
	eBtCmdDisablePasskey, /* 关闭PasskeyEntry配对功能（拉卡拉版本不支持）*/ 
	eBtCmdGetPasskeyPairRequest, /* 查询PasskeyEntry配对请求（拉卡拉版本不支持）*/ 
	eBtCmdSetPasskeyPairPassword, /* 设置PasskeyEntry配对密码（拉卡拉版本不支持）*/ 
	eBtCmdDisableYesNo,	 /*关闭YesNo配对功能for Brazil */
	eBtCmdEnableYesNo,	 /*打开YesNo配对功能for Brazil */
	eBtCmdGetYesNoPairRequese,/*查询YesNo配对功能 for Brazil */
	eBtCmdSetYesNoPairRequese /*设置YesNo配对功能 for Brazil */
}E_BtCtlCmd;
//MINI
typedef struct {
	unsigned char name[64];
	unsigned char pin[16];
	unsigned char mac[6];
	unsigned char RFU[10];
} ST_BT_CONFIG;

typedef struct {
	unsigned char name[64];
	unsigned char mac[6];
	unsigned char RFU[10];
}ST_BT_SLAVE;

typedef struct {
	unsigned char status;
	unsigned char name[64];
	unsigned char mac[6];
}ST_BT_STATUS;

typedef struct{
	unsigned char base_bt_exist; // 0表示座机不存在蓝牙 1表示存在蓝牙
	unsigned char base_bt_mac[6];  // 座机蓝牙mac
	unsigned char base_bt_pincode[16]; // 座机蓝牙密码
	unsigned char base_bt_name[20];			// 最多只取前面20字节
}ST_BT_BASE_INFO;
//#define eBtUploadBaseBtInfoCmd 7

typedef enum
{
	PINCODE_MODE = 0, 
	JUSTWORKS_MODE,    
	PASSKEY_ENTRY_MODE,    
	NUMERIC_COMPARISON_MODE,
}BT_PAIRING_MODE_E;

typedef struct
{
	BT_PAIRING_MODE_E mode;
}ST_BT_PAIRING_MODE;

typedef struct
{
	BT_PAIRING_MODE_E mode;
	unsigned char key[16];
}ST_BT_SET_PAIRING_RESP, ST_BT_GET_PAIRING_REQ;


typedef enum
{
	DISABLE_BROADCAST = 0,
	ENABLE_BROADCAST,
}BT_BROADCAST_STATUS_E;

typedef struct
{
	BT_BROADCAST_STATUS_E status;
}ST_BT_SET_BROADCAST_STATUS;

typedef struct
{
	char version[64];
}ST_BT_VERION;

typedef enum
{
	eBtGetPairingModeCmd = 1,
	eBtSetPairingModeCmd,
	eBtGetPairingReqCmd,
	eBtSetPairingRespCmd,
	eBtSetBroadcastStatusCmd,
	eBtGetVersionCmd,
	eBtUploadBaseBtInfoCmd = 7,
}BT_CTRL_CMD_E;


//=================================================

//================================================
//            Wifi functions
//================================================
//wifi
#define WIFI_ROUTE_NUM 12

#define WLAN_SEC_UNSEC (0)
#define WLAN_SEC_WEP    (1)
#define WLAN_SEC_WPA_WPA2   (2)
#define WLAN_SEC_WPAPSK_WPA2PSK (3)
#define WLAN_SEC_WPAPSK (4)
#define WLAN_SEC_WPA2PSK (5)
#define IPLEN 4

#define KEY_WEP_LEN_MAX 16
#define KEY_WEP_LEN_64 5
#define KEY_WEP_LEN_128 13
#define KEY_WEP_LEN_152 16

#define KEY_WEP_LEN 5
#define KEY_WPA_MAXLEN 63
#define SSID_MAXLEN 32
#define SCAN_AP_MAX 15

#define WIFI_RET_OK 				0 
#define WIFI_RET_ERROR_NODEV 		-1 
#define WIFI_RET_ERROR_NORESPONSE 	-2 
#define WIFI_RET_ERROR_NOTOPEN 		-3 
#define WIFI_RET_ERROR_NOTCONN 		-4 
#define WIFI_RET_ERROR_NULL 		-5 
#define WIFI_RET_ERROR_PARAMERROR 	-6 
#define WIFI_RET_ERROR_STATUSERROR 	-7 
#define WIFI_RET_ERROR_DRIVER	-8
/*密码认证错误*/
#define WIFI_RET_ERROR_AUTHERROR -9 
/*扫描AP异常*/
#define WIFI_RET_ERROR_NOAP -10
/*设置或获取IP失败*/
#define WIFI_RET_ERROR_IPCONF -11
#define WIFI_RET_ERROR_NOTSUPPORT -13
#define WIFI_RET_ERROR_DHCP_NOENABLE -16
#define WIFI_RET_ERROR_DHCP_ENABLE -17

typedef struct
{
    char fw_fname[16];      //文件名,for all
    char soft_ver[64];      //返回版本号
    char reserve[64];       //reserve for future use.  
}WL_UPGRADE_INFO;

typedef struct 
{ 
    uchar Key[4][KEY_WEP_LEN_MAX]; /* WEP 密码数据 */ 
    int KeyLen; /* WEP 密码长度 5 or 13 or 16*/ 
    int Idx; /* WEP密钥索引[0..3] */ 
} ST_WEP_KEY;

typedef struct
{
    int DhcpEnable; //DHCP使能，0-关闭，1-开启
    uchar Ip[IPLEN];    //设置静态IP
    uchar Mask[IPLEN];  //掩码
    uchar Gate[IPLEN];  //网关
    uchar Dns[IPLEN];   //DNS
    ST_WEP_KEY Wep; //wep密码
    uchar Wpa[KEY_WPA_MAXLEN];  //WPA连接密码
    uchar reserved[256];//预留
}ST_WIFI_PARAM;

typedef struct
{
	int DhcpEnable;	//DHCP使能，0-关闭，1-开启
	uchar Ip[IPLEN];	//设置静态IP
	uchar Mask[IPLEN];	//掩码
	uchar Gate[IPLEN];	//网关
	uchar Dns[IPLEN];	//DNS
	uchar Wep[KEY_WEP_LEN];	//wep密码
	uchar Wpa[KEY_WPA_MAXLEN];	//WPA连接密码
	uchar reserved[256];//预留
}ST_WIFI_PARAM_MINI;

typedef struct 
{
    uchar Ssid[SSID_MAXLEN];        //搜索到的AP的SSID
    int SecMode;    //安全模式
    int Rssi;   //搜索到的AP的信号强度
}ST_WIFI_AP;

typedef struct 
{
	ST_WIFI_AP  stBasicInfo;
	unsigned char Bssid[6];
	unsigned int bssType;//0：表示未知，1：表示Infrastrucure, 2：表示ad-hoc
    unsigned int channel; //0：表示未知，其他：表示AP当前使用信道号
unsigned char reserved[124];
} ST_WIFI_AP_EX;


//================================================
//            固定电话相关定义
//================================================
#define MAX_MDHM                         10
#define MAX_TELNUM            64 
#define MAX_USENAME          64

typedef struct
{
	uchar        FskTxlevel;/*[0~7] modem FSK发送数据时的电平值*/
	uchar        FskRxlevel;/*[0~7] modem FSK接收数据时的电平值*/
	uchar        FskMarkTime;/*[2~30][单位:10MS] MODEM fsk通信时 MARK(标志位)持续时长*/
	uchar        CidFskGate;/*[0~7] 接收FSK来电显示信息 (Caller ID) 时的门限电平值*/
	uchar        CidDtmfGate;/*[0~7] 接收DTMF来电显示信息时的门限电平值*/
	uchar        DtmfOnTime;/*[50~255][单位:10MS] DTMF拨号时每个发号码元的持续时长 ;默认0为10 ,代表100毫秒 */
	uchar        DtmfOffTime;/*[0~255][单位:10MS] DTMF拨号时两发号码元之间的间隔时长；默认0为10 ,代表100毫秒 */
	uchar        PauseButtonTime;/*[0~255][单位:100MS] 电话暂停按键的暂停时长*/
	uchar        FirstRingMute;/*1,第一声铃声静音,第二声开始响铃;0,正常响铃*/
	/* Reserved[0]:[0~7]拨号音检测门限电平 */
	/* Reserved[1~49]:保留  */
	/* Reserved[50]:[5~255][单位:100ms]TelDial()摘机到拨号之间的延时 */
	uchar        Reserved[51];/* 保留 */    
}TEL_PARA;

typedef struct
{
	uchar HandlePicked;/* 1,检测到手柄摘机;0,手柄挂机 */   
	uchar RingFound;/* 1,检测到振铃;0,未检测到振铃 */
	uchar CidFound;/* 1,已接收到来电显示信息,可以读取来电;0,无来电 */
	uchar CidTimeStr[10];/* 月日时分:30 35 32 36 30 39 34 39表示05月26日9点49分*/
	uchar CidTelNo[64];/* 来电显示号码,有效值{0123456789ABCDEFPOpo} */
	uchar CidUserName[64];/* 用户信息,有些来电没有该信息*/
}TEL_STATUS;

//================================================
//            GPS相关的定义
//================================================
//成功
#define GPS_RET_OK	0
//	正在获取导航数据	
#define GPS_RET_NAVIGATING	-1
//	设备错误
#define GPS_RET_ERROR_NODEV	-2
//	GPS模块无响应
#define GPS_RET_ERROR_NORESPONSE	-3
//	GPS模块未打开
#define GPS_RET_ERROR_NOTOPEN	-4
//	参数为空
#define GPS_RET_ERROR_NULL	-5
//	参数错误
#define GPS_RET_ERROR_PARAMERROR	-6
//	获取导航数据失败
#define GPS_RET_ERROR_NAVIGATE_FAIL	-7

typedef struct {
    double latitude;
    double longitude;
    double altitude;
} GpsLocation;

//================================================
//            颜色相关的定义
//================================================
//dll编译时需要屏蔽，但是拷贝到include目录时需要屏蔽这两个定义
#ifndef _PROPAY_
	typedef int COLORREF;
	#define RGB(r, g, b) ((COLORREF)((((unsigned int)((r&0xF8) >> 3)) << 11) | (((unsigned int)((g&0xFC) >> 2)) << 5) | ((unsigned char)((b&0xFC) >> 3)))) 
#endif

#define RGB_R(rgb) ((unsigned char)(((rgb)>>11) << 3)) 
#define RGB_G(rgb) ((unsigned char)((rgb)>>5) << 2) 
#define RGB_B(rgb) ((unsigned char)((rgb) << 3)) 
#ifndef NO_FILL_COLOR
#define NO_FILL_COLOR 0xffffffff
#endif

//================================================
//            QR68 4G电池相关系统函数定义
//================================================
#define STORAGE_DATA 7

//struct batt_status_info{
//unsigned char ext_pwr_pres;
//unsigned char batt_pres;
//unsigned char chrg_mode;
//unsigned char batt_level;
//unsigned char excep_type;
//unsigned short voltage;
//short current;
//unsigned short temperature;
//unsigned char soc;
//unsigned char ex_info[128];
//};

struct  batt_status_info
{
	/* Adapter or Base is present or not, 0: absence; 1: present*/
	/* 适配器或底座是否在位，0：不在位；1：在位 */
	unsigned char ext_pwr_pres;
	/* Battery is present or not, 0: absence; 1: present */
	/* 电池是否在位：0：不在位；1：在位*/
	unsigned char batt_pres;
	/* Current mode, 1: Mobile Mode; 2 Counter-top Mode */
	/* 当前模式，1：移动模式；2：桌面模式 */
	unsigned char chrg_mode;
	/* Battery level , 0: battery voltage low and battery icon blinks;
	1~4: battery icon displays 1~4 grid(s);
	5:external power supply is provided and battery is in charging;
    6: external power supply is provided and battery charging is finished. 
    */
	/* 电池图标格数：0:电池电量低，电池图标闪烁中；
    1~4：电池图标分别显示1~4格；
    5：外电插入，电池充电中；
    6：外电插入，电池充电完成 */
	unsigned char batt_level;
	/* Exception type, 1: charging exception; 2:charging-over-voltage exception;
     3:charging-timeout exception 
    */
	unsigned char excep_type;
	/* Current voltage of battery, in unit of mV */
   /* 电池当前电压，单位mV */
	unsigned short voltage;
	/* Current current of battery, in unit of mA.
	   Negative value indicates that battery is in discharging;
	   Positive value indicates battery in charging;
	   Zero indicates that battery is neither in charging nor discharging, 
	   and power supply is provided by external adapter.
	*/
    /* 电池当前电流，单位mA; 负值表示没有外电时电池的放电电流；
	   正值表示有适配器电池的充电电流；0表示电池没有放电也没有充电、
	   由适配器供电。
    */
	short current;
	/* Current temperature of battery, in unit of 0.1 degree */
	/* 电池当前温度，单位0.1度*/
	short temperature;
	/* State of Charge, i.e relative capacity, in unit of % */
	/* 电池相对剩余容量，单位% */
	unsigned char soc;
	/* 电池的充-放电次数 */
	unsigned short cyc_cnt;
	/* 电池的健康状况，单位% */
	unsigned char soh;
	/* */
	unsigned char ex_info[124];
};



#include "posapi_p78.h"
#include "posapi_p80.h"
#include "posapi_p90.h"
#include "posapi_s80.h"
#include "posapi_all.h"
#include "posapi_s78.h"
#ifdef PROLIN_OS_APP
	#include "../postype/QR68/include/posapiaddtion.h"
#endif

#ifdef PROLIN_OS_APP
	#pragma pack(pop)
#endif

#ifdef __cplusplus
extern "C"
{
#endif

	uchar POS_API _getkey(void);
	void POS_API PiccLight(unsigned char ucLedIndex,unsigned char ucOnOff);

	/************************************************************\
	*															*
	* the following functions or variables are not implemented  *
	* or defined in the lib. it must be defined in the project	*
	* which will use the lib.									*
	*															*
	\************************************************************/
	extern const APPINFO AppInfo;
	extern int event_main(ST_EVENT_MSG *msg);
	extern int main();

#ifdef __cplusplus
};
#endif


#endif
