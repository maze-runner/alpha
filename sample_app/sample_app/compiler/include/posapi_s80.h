/*****************************************************/
/*              API for S80                          */
/*****************************************************/
#ifndef  _POSAPI_S80_H_
#define  _POSAPI_S80_H_

//PICC LED显示控制宏
#define PICC_LED_RED    0x01  
#define PICC_LED_GREEN  0x02
#define PICC_LED_YELLOW 0x04 
#define PICC_LED_BLUE   0x08

//================================================
//         FAT API  
//================================================

/*
** 错误码
**/
#define FS_OK                0/* no error, everything OK! */
#define FS_ERR_OTHERS		 -1 /*other erros*/
#define FS_ERR_NODEV         -5 /* 所操作的设备不存在*/
#define FS_ERR_BUSY          -6 /* 节点不能同时打开两次*/
#define FS_ERR_ARG           -7/* 错误参数*/
#define FS_ERR_EOF           -8 /* 已到尾部*/
#define FS_ERR_NAME_SIZE     -9/* 名字太长*/
#define FS_ERR_NAME_EXIST    -10/* 名字已存在*/
#define FS_ERR_CHAR          -11/* 存在非法字符*/
#define FS_ERR_NAME_SPACE    -12/* short name NUM is full*/
#define FS_ERR_DISK_FULL     -13/* DISK SPACE is full */
#define FS_ERR_ROOT_FULL     -14/* root space is full */
#define FS_ERR_CACHE_FULL    -15/* cache full, please submit */
#define FS_ERR_PATH          -16/* path depth */
#define FS_ERR_NOENT         -17/* no such inode */
#define FS_ERR_NOTDIR        -18/* not dir */
#define FS_ERR_ISDIR         -19/* is dir */
#define FS_ERR_NOMEM         -20/* NO MEMORY */
#define FS_ERR_BADFD         -21/* BAD file descriptor */
#define FS_ERR_NOTNULL       -22/* dir not null */
#define FS_ERR_WO            -23/* write only */
#define FS_ERR_RO            -24/* read only */
#define FS_ERR_SYS           -25/* sys not support */
#define FAT_ERROR_MAX        -60

/*
** 路径最大长度
**/
#define PATH_NAME_MAX 2*1024

#define NAME_FMT_UNICODE 0x1/** UNICODE ，目前不支持*/
#define NAME_FMT_ASCII 0x2 /** ASCII 编码，合法字符0x20~0x7d*/

/** File System Width STRing */
typedef struct
{
	char* str;				/* 指向存储空间的指针 */
	int  size;				/* 字符串存储空间 */
	int  fmt;		/* 编码格式，取值为NAME_FMT_ASCII */
}FS_W_STR;

#define FS_ATTR_C 0x1 /* Create if not exist*/
#define FS_ATTR_D 0x2 /* inode is DIR */
#define FS_ATTR_E 0x4/* exclusive */
#define FS_ATTR_R 0x8 /* Read */
#define FS_ATTR_W 0x10 /* Write */
#define FS_ATTR_CRWD (FS_ATTR_C|FS_ATTR_R|FS_ATTR_W|FS_ATTR_D)
#define FS_ATTR_RW   (FS_ATTR_R|FS_ATTR_W)
#define FS_ATTR_RWD  (FS_ATTR_R|FS_ATTR_W|FS_ATTR_D)

#define LONG_NAME_MAX (200*2)
#define SHORT_NAME_MAX (11+1)

#define ATTR_RO 0x01  /* read only */ 
#define ATTR_HID 0x02 /* hidden */
#define ATTR_SYS 0x04 /* system file */
#define ATTR_VOL 0x08 /* volume ID */
#define ATTR_DIR 0x10 /* directory */
#define ATTR_ARC 0x20 /* archive */

typedef struct fs_date_time_s
{
	long seconds;
	long minute;
	long hour;
	long day;
	long month;
	long year;
}FS_DATE_TIME;

typedef struct
{
	FS_DATE_TIME    wrtime;/* lastly write time */
	FS_DATE_TIME    crtime;/* create time */
	int             size;/* file size */
	int             space;/* space size */
	int             name_fmt;
	int             attr;/* 属性*/
	int             name_size;/* name的有效长度 */
	char            name[LONG_NAME_MAX+4];
}FS_INODE_INFO;

typedef enum {
	FS_SEEK_SET=1,
	FS_SEEK_CUR,
	FS_SEEK_END,
}FS_SEEK_FLAG;


#define FS_VER_FAT16 0
#define FS_VER_FAT32 1

typedef struct
{
	int ver;/* 版本号:FS_VER_FAT16 or FS_VER_FAT32 */
	int free_space;/* 空闲空间,单位字节*/
	int used_space;/* 已用空间,单位字节*/
}FS_DISK_INFO;

//================================================
//           Error code for (GPRS & CDMA) API
//================================================
#define WNET_RET_OK			0
//模块口被占用
#define ERR_WNET_PORTINUSE 	-1
//模块没有应答
#define ERR_WNET_NORSP		-2
//模块应答错误
#define ERR_WNET_RSPERR		-3
//模块串口没有打开
#define ERR_WNET_PORTNOOPEN -4
//需要PIN码
#define ERR_WNET_NEEDPIN	-5
//需要PUK解PIN码
#define ERR_WNET_NEEDPUK	-6
//参数错误
#define ERR_WNET_PARAMER	-7
//密码错误
#define ERR_WNET_ERRPIN		-8
//没有插入SIM卡
#define	ERR_WNET_NOSIM      -9
//不支持的类型
#define ERR_WNET_NOTYPE		-10
//网络没有注册
#define ERR_WNET_NOREG		-11
//模块曾初始化
#define ERR_WNET_INIT_ONCE	-12
//没有连接
#define ERR_WNET_NOCONNECT  -13
//线路断开
#define ERR_WNET_LINEOFF	-14
//没有socket可用
#define ERR_WNET_NOSOCKETUSE	-15
//超时
#define ERR_WNET_TIMEOUT		-16
//正在拨号中
#define ERR_WNET_CALLING 		-17
//重复的socket请求
#define ERR_WNET_REPEAT_SOCKET	-18
//socket 已经断开
#define ERR_WNET_SOCKET_DROP	-19
//socket 正在连接中
#define ERR_WNET_CONNECTING     -20
//socket 正在关闭
#define ERR_WNET_SOCKET_CLOSING	-21
//网络注册中
#define ERR_WNET_REGING			-22
//关闭串口出错
#define ERR_WNET_PORTCLOSE  	-23



//================================================
//         Net API  for (ETH,PPP)       
//================================================

//Error Code 
#define NET_OK   	 0      /* No error, everything OK. */
#define NET_ERR_MEM  -1      /* Out of memory error.     */
#define NET_ERR_BUF  -2      /* Buffer error.            */
#define NET_ERR_ABRT -3      /* Connection aborted.      */
#define NET_ERR_RST  -4      /* Connection reset.        */
#define NET_ERR_CLSD -5      /* Connection closed.       */
#define NET_ERR_CONN -6      /* Not connected.           */
#define NET_ERR_VAL  -7      /* Illegal value.           */
#define NET_ERR_ARG  -8      /* Illegal argument.        */
#define NET_ERR_RTE  -9      /* Routing problem.         */
#define NET_ERR_USE  -10     /* Address in use.          */
#define NET_ERR_IF   -11     /* Low-level netif error    */
#define NET_ERR_ISCONN -12   /* Already connected.       */
#define NET_ERR_TIMEOUT -13  /* time out */
#define NET_ERR_AGAIN  -14   /*no block, try again */
#define NET_ERR_EXIST  -15   /* exist already */
#define NET_ERR_SYS    -16   /* sys don not support */

/* PPP ERROR code */
#define NET_ERR_PASSWD -17 /* error password */
#define NET_ERR_MODEM  -18 /* modem dial is fail */
#define NET_ERR_LINKDOWN -19 /* ppp link disconnect */

#define NET_AF_INET 1

enum
{
	NET_SOCK_STREAM=1,	/* TCP */
	NET_SOCK_DGRAM		/* UDP */
};


typedef struct  net_in_addr
{
	unsigned long NET_s_addr;
}NET_IN_ADDR;

typedef struct net_sockaddr_in
{
	char  sin_len;
	char  sin_family;
	short sin_port;
	struct net_in_addr sin_addr;
	char sin_zero[8];
}NET_SOCKADDR_IN;

typedef struct net_sockaddr
{
	char sa_len;
	char sa_family;
	char sa_data[14];
}NET_SOCKADDR;

#ifndef socklen_t
#define socklen_t int
#endif


#define NOBLOCK 1
#define BLOCK   0/* 阻塞标志位 */
#define SOCK_EVENT_READ    (1<<0)
#define SOCK_EVENT_WRITE   (1<<1)
#define SOCK_EVENT_CONN    (1<<2)
#define SOCK_EVENT_ACCEPT  (1<<3)
#define SOCK_EVENT_ERROR   (1<<4)
#define SOCK_EVENT_MASK    (0xff)

enum
{
	CMD_IO_SET=1,
	CMD_IO_GET,
	CMD_TO_SET,/* timeout set */
	CMD_TO_GET,/* timeout get */
	CMD_IF_SET,/* net_socket bind dev IF (interface)*/
	CMD_IF_GET,/* get the dev if net_socket bind */
	CMD_EVENT_GET,/* get event , such as SOCK_EVENT_READ,SOCK_EVENT_WRITE, etc*/
	CMD_BUF_GET,/* get send or recv buffer , only for SOCK_STREAM */
	CMD_FD_GET,/* get free net_socket fd num */
	/*2010-07-21 add*/
	CMD_KEEPALIVE_SET,/*配置sock的KeepAlive功能，该命令只对NET_SOCK_STREAM有效*/
	CMD_KEEPALIVE_GET,/*获取sock的KeepAlive情况，该命令只对NET_SOCK_STREAM有效*/
};

/* 网络设备状态标志位*/
#define FLAGS_UP 0x1 /* link state OK */
#define FLAGS_TX 0x2 /* can tx */
#define FLAGS_RX 0x4 /* can rx */


/* arg for CMD_BUF_GET */
#define TCP_SND_BUF_MAX 1
#define TCP_RCV_BUF_MAX 2
#define TCP_SND_BUF_AVAIL 3

/* 认证算法定义宏*/
#define PPP_ALG_PAP       0x1 /* PAP */
#define PPP_ALG_CHAP      0x2 /* CHAP */
#define PPP_ALG_MSCHAPV1  0x4 /* MS-CHAPV1 */
#define PPP_ALG_MSCHAPV2  0x8 /* MS-CHAPV2 */
#define PPP_ALG_ALL 	  0xff /*支持所有的认证算法*/


#define ETH_ROUTE  0
#define PPP_ROUTE  10



#define	PCI_OK				0x00	//正确
#define	PCI_ERROR			0x01	//其它错误
#define	PCI_PEDAAKEY_ERR	0x03	//PED不存在本应用应用名
#define	PCI_KEYINDEX_ERR	0x05	//密钥索引号非法
#define	PCI_NO_KEY			0x06	//密钥不存在
#define	PCI_KEYAUTH_ERR		0x07    //非本应用密钥(其它应用)	
#define	PCI_KEYLEN_ERR		0x08	//密钥长度错误
#define	PCI_CANCEL			0x09	//用户取消
#define	PCI_TIMEOUT			0x0a	//超时退出
#define	PCI_WAIT_INTERVAL	0x0b	//两次调用本函数时间间隔不足
#define	PCI_ICC_NOCARD		0x0c	//卡不在位
#define	PCI_ICC_NOTINIT		0x0d	//卡未上电
#define	PCI_COMM_ERR		0x0e	//通讯错误
#define	PCI_NEED_ADMIN		0x0f	//需要先取得高级权限才能操作
#define	PCI_LOCKED			0x10	//PED已经锁定，请先解锁
#define	PCI_NO_PIN			0x11	//PIN输入时用户直接按确认（无密码
#define	PCI_NO_UPK			0x12    //DUKPT OVER FLOW
#define	PCI_PED_NOMORE_BUF	0XFE
#define PCI_ADMIN_ERR		0x14;    //获取系统敏感服务失败
#define	PCI_NOMORE_KEY		0XB0	//key空间已满
#define	PCI_DUKPT_OVERFLOW	0xB1	//
#define	PCI_UNSPT_CMD 		0xff	//命令不支持

#define	TYPE_MASTERKEY	0x00
#define	TYPE_PINKEY		0X01
#define	TYPE_MACKEY		0x02
#define	TYPE_DUKPTKEY	0X03

//#define	PED_ENCRYPT		0X01// posapi.h(241)
//#define	PED_DECRYPT 	0X00// posapi.h(240)

typedef struct
{
     uint  modlen;          //PIN加密公钥模数长
     uchar mod[256];        //PIN加密公钥模数,不足位前补0x00
     uchar exp[4];          //PIN加密公钥指数,不足位前补0x00
     uchar iccrandomlen;    //从卡行取得的随机数长
     uchar iccrandom[8];    //从卡行取得的随机数
}RSA_PINKEY;

// **********************************************
//		Font
// **********************************************


//最多可支持的字体数
#define MAX_FONT_NUMS       32

//可支持的最大字体宽度和高度
#define MAX_FONT_WIDTH      32
#define MAX_FONT_HEIGHT     32

//字符集定义
#define CHARSET_WEST        0x01      //美国、英国及西欧国家        
#define CHARSET_TAI         0x02      //泰国                        
#define CHARSET_MID_EUROPE  0x03      //中欧                           
#define CHARSET_VIETNAM     0x04      //越南                           
#define CHARSET_GREEK       0x05      //希腊                           
#define CHARSET_BALTIC      0x06      //波罗的海                       
#define CHARSET_TURKEY      0x07      //土耳其                         
#define CHARSET_HEBREW      0x08      //希伯来                          
#define CHARSET_RUSSIAN     0x09      //俄罗斯                        
#define CHARSET_GB2312      0x0A      //简体中文      
#define CHARSET_GBK         0x0B      //简体中文     
#define CHARSET_GB18030     0x0C      //简体中文        
#define CHARSET_BIG5        0x0D      //繁体中文        
#define CHARSET_SHIFT_JIS   0x0E      //日本                          
#define CHARSET_KOREAN      0x0F      //韩国                           
#define CHARSET_ARABIA      0x10      //阿拉伯	                          
#define CHARSET_DIY         0x11      //自定义字体           

//字体结构定义
typedef struct{
	int CharSet;    //字符集
	int Width;      //字体宽度
	int Height;     //字体高度
	int Bold;       //（1：黑体， 0：正常）
	int Italic;     //（1：斜体， 0：正常）
}ST_FONT;


// **********************************************
//		USB
// **********************************************

#define USBDEV_READ   0x1
#define USBDEV_WRITE  0x2
/*
** 错误码
**/
#define USB_OK             0/* no error, everything OK! */
#define USB_ERR_NOCONF   -10/* 没有配置成功*/
#define USB_ERR_TIMEOUT  -11/* 超时*/
#define USB_ERR_NOINIT   -12/* 模块没有初始化成功*/
#define USB_ERR_NOSTART  -13/* 没有启动Device功能*/
#define USB_ERR_START    -14/* 已启动Device功能*/
#define USB_ERR_PACKET   -15/* 收到的对方报文不完整*/
#define USB_ERR_DATA     -16/* 收到的对方数据不完整*/
#define USB_ERR_DOING    -17/* */
#define USB_ERR_BUF      -18
#define USB_ERR_DISCONN  -19/* 没有连接到Host*/
#define USB_ERR_PARAM    -20/* bad param */



#if (defined _POS_S80)	|| (defined _POS_SP30)	// for S80 pos and SP30

#ifdef __cplusplus
extern "C" {
#endif

//=================================================
//               System basic functions			   
//=================================================
uchar SystemInit(void);
void  Beep(void);
void  Beef(uchar mode,ushort DlyTime);
uchar SetTime(uchar *time);
void  GetTime(uchar *time);
void  DelayMs(ushort Ms);
void  TimerSet(uchar TimerNo, ushort Cnts);
ushort TimerCheck(uchar TimerNo);
void  ReadSN(uchar *SerialNo);
void  EXReadSN(uchar *SN);
uchar ReadVerInfo(uchar *VerInfo);
int   GetTermInfo(uchar *out_info);
int   GetLastError(void);

//=================================================
//		    Encrypt/Decrypt functions
//=================================================
void des(uchar *input, uchar *output,uchar *deskey, int mode);
void Hash(uchar* DataIn, uint DataInLen, uchar* DataOut);
int  RSARecover(uchar *pbyModule,uint dwModuleLen,uchar *pbyExp, 
			   uint dwExpLen,uchar *pbyDataIn,uchar *pbyDataOut);

//================================================
//                 Keyboard functions
//================================================
uchar kbhit(void);
void  kbflush(void);
uchar getkey(void);
void  kbmute(uchar flag);
uchar GetString(uchar *str,uchar mode,uchar minlen,uchar maxlen);
uchar GetHzString(uchar *outstr, uchar max, ushort TimeOut);

//==================================================
//					LCD functions
//==================================================
void  ScrCls(void);
void  ScrClrLine(uchar startline, uchar endline);
void  ScrGray(uchar mode);
void  ScrBackLight(uchar mode);
void  ScrGotoxy (uchar x,uchar y);
uchar ScrFontSet(uchar fontsize);
uchar ScrAttrSet(uchar Attr);
int   Lcdprintf(const char *fmt,...);
void  ScrPrint(uchar col,uchar row,uchar mode, char *str,...);
void  ScrPlot(uchar X, uchar Y, uchar Color);
void  ScrDrLogo(uchar *logo);
void  ScrDrLogoxy(int x,int y,uchar *logo);
uchar ScrRestore(uchar mode);
void  ScrSetIcon(uchar icon_no,uchar mode);

//=================================================
//			   Magcard reader functions
//=================================================
void  MagOpen(void);
void  MagClose(void);
void  MagReset(void);
uchar MagSwiped(void);
uchar MagRead(uchar *Track1, uchar *Track2, uchar *Track3);

//=================================================
//			   Smart card reader functions
//=================================================
uchar IccInit(uchar slot,uchar *ATR);
void  IccClose(uchar slot);
void  IccAutoResp(uchar slot, uchar autoresp);
uchar IccIsoCommand(uchar slot, APDU_SEND *ApduSend, APDU_RESP *ApduRecv);
uchar IccDetect(uchar slot);

//=================================================
//			   RF card functions
//=================================================
uchar PiccOpen(void);
//uchar PiccInfoSetup (uchar mode, PICC_PARA_INFO *picc_para);	// mich_temp: protype different
uchar PiccSetup (uchar mode, PICC_PARA *picc_para);
uchar PiccDetect(uchar Mode,uchar *CardType,uchar *SerialInfo,uchar *CID,uchar *Other);
uchar PiccIsoCommand(uchar cid,APDU_SEND *ApduSend,APDU_RESP *ApduRecv);
uchar PiccRemove(uchar mode,uchar cid);
void PiccClose(void);
uchar M1Authority(uchar Type,uchar BlkNo,uchar *Pwd,uchar *SerialNo);
uchar M1ReadBlock(uchar BlkNo,uchar *BlkValue);
uchar M1WriteBlock(uchar BlkNo,uchar *BlkValue);
uchar M1Operate(uchar Type,uchar BlkNo,uchar *Value,uchar UpdateBlkNo);
void PiccLEDCtrl(uchar Mode);

//================================================
//                Printer functions
//================================================
uchar PrnInit(void);
void  PrnFontSet(uchar Ascii, uchar CFont);
void  PrnSpaceSet(uchar x, uchar y);
void  PrnStep(short pixel);
uchar PrnStr(char *str,...);
uchar PrnLogo(uchar *logo);					// mich_temp: protype different
uchar PrnStart(void);
uchar PrnStatus(void);
void  PrnLeftIndent(ushort Indent);
int PrnGetDotLine(void);
void PrnSetGray(int Level);
int  PrnGetFontDot(int FontSize, uchar *str, uchar *OutDot);
// new api for S-serials pos
int  PrnSelectFont(ST_FONT *psingle_code_font, ST_FONT *pmulti_code_font);
void PrnDoubleWidth(int AscDoubleWidth, int LocalDoubleWidth);
void PrnDoubleHeight(int AscDoubleHeight, int LocalDoubleHeight);
void PrnAttrSet(int reverse);

//================================================
//                PCI PED functions
//================================================
void PciGetRandom(uchar *random);
uchar PciWritePinKey(ushort  MKeyID, ushort PinKeyID, uchar PinKeyLen, uchar *PinKey);
uchar PciWriteMacKey(ushort  MKeyID, ushort MacKeyID, uchar MacKeyLen, uchar *MacKey);
uchar PciGetPin(ushort PinKeyID, uchar *ExpectPinLen, uchar *CardNo, uchar Mode, uchar *PinBlock);
uchar PciGetMac(ushort MacKeyID, uchar *DataIn, ushort InLen, uchar Mode, uchar *MacOut);
uchar PciGetPinDukpt(uchar *ExpectPinLen, uchar *CardNo, uchar Mode, uchar *PinBlock, uchar *OutKsn);
uchar PciGetMacDukpt(uchar *DataIn, ushort InLen, uchar Mode, uchar *MacOut, uchar *OutKsn);
uchar PciGetVerifyPlainPinOffLine(uchar IccSlot, uchar *ExpectPinLen, uchar *IccCmd, uchar *IccResp);
uchar PciGetVerifyCipherPinOffLine(uchar IccSlot, uchar *ExpectPinLen, RSA_PINKEY *RsaPinKey, uchar *IccCmd, uchar *IccResp);

uchar PciSetTimeOut(ulong ulTimeOutMs,ulong ulGetPinTimeMs,ulong ulGetMacTimeMs);
uchar PciDerivePinKey(uchar MSrcKeyType,ushort MSrcKeyID, ushort  SrcPinKeyID, ushort DstPinKeyID,uchar ucMode);
uchar PciDeriveMacKey(uchar MSrcKeyType,ushort MSrcKeyID, ushort  SrcMacKeyID, ushort DstMacKeyID,uchar ucMode);
uchar PciWriteMasterKey(ushort MKeyID, uchar MKeyLen, uchar *MKey);
uchar PciWriteDukptInitKey(uchar InitKeyLen, uchar *InitKey, uchar* InKsn);
uchar PciWriteDesKey(ushort DKeyID, uchar DKeyLen, uchar *DKey);
uchar PciDesKeyTdes(ushort DKeyID,uchar Mode,uchar* DataIn,uchar* DataOut);
//================================================
//       Multiple apps management functions
//================================================
int   ReadAppInfo(uchar AppNo, APPINFO* ai);
int   RunApp(uchar AppNo);
int   DoEvent(uchar AppNo, ST_EVENT_MSG *msg);

//================================================
//       File functions
//================================================
int   open(char *filename, uchar mode);
int   ex_open(char *filename, uchar mode,uchar* attr);
int   read(int fid, uchar *dat, int len);
int   write(int fid, uchar *dat, int len);
int   close(int fid);
int   seek(int fid, long offset, uchar fromwhere);
long  filesize(char *filename);
int   remove(const char *filename);
long  freesize(void);
int   truncate(int fid,long len);
int   fexist(char *filename);
int   GetFileInfo(FILE_INFO* finfo);

//================================================
//       Enviromental variable functions
//================================================
uchar GetEnv(char *name, uchar *value);
uchar PutEnv(char *name, uchar *value);

//================================================
//       FAT file system functions
//================================================
int   FsOpen(FS_W_STR *name, int attr);
int   FsClose(int fd);
int   FsDelete(FS_W_STR *name_in);
int   FsGetInfo(int fd, FS_INODE_INFO *fs_inode);
int   FsRename(FS_W_STR * name, FS_W_STR *new_name);
int   FsDirRead(int fd, FS_INODE_INFO *fs_inode, int num);
int   FsDirSeek(int fd, int num, int flag);
int   FsFileRead(int fd, char *buf, int len);
int   FsFileWrite(int fd, char *buf, int len);
int   FsFileSeek(int fd, int offset, int flag);
int   FsFileTell(int fd);
int   FsFileTruncate(int fd, int size, int reserve_space);
int   FsSetWorkDir(FS_W_STR *name);
int   FsGetWorkDir(FS_W_STR *name);
int   FsUdiskIn(void);
int   FsGetDiskInfo(int disk_num, FS_DISK_INFO *disk_info);

//================================================
//     Asynchronism communication functions
//================================================
uchar PortOpen(uchar channel, uchar *para);
uchar PortClose(uchar channel);
uchar PortSend(uchar channel, uchar ch);
uchar PortRecv(uchar channel, uchar *ch, ushort ms);
uchar PortReset(uchar channel);
uchar PortSends(uchar channel,uchar *str,ushort str_len);
uchar PortTxPoolCheck(uchar channel);

//================================================
//		Modem communication functions
//================================================
uchar ModemReset(void);
uchar ModemDial(COMM_PARA *MPara, uchar *TelNo, uchar mode);
uchar ModemCheck(void);
uchar ModemTxd(uchar *SendData, ushort len);
uchar ModemRxd(uchar *RecvData, ushort *len);
uchar ModemAsyncGet(uchar *ch);
uchar OnHook(void);
ushort ModemExCommand(uchar *CmdStr, uchar *RespData,
					  ushort *Dlen,ushort Timeout10ms);

//================================================
//	     Wireless network functions
//================================================
int   ExWNetInit(const uchar *Pin);
int   ExWNetOpen();
int   ExWNetCheckSignal(uchar *SignalLevel);
int   ExWNetPPPDial(const uchar *DialNum,const uchar *Uid,
					const uchar *Pwd,void *Reserved);
int   ExWNetPPPClose();
int   ExWNetTcpConnect(int * Socket,const uchar * DestIP,ushort DestPort);
int   ExWNetTcpCheck(int Socket);
int   ExWNetTcpClose(int Socket);
int   ExWNetTcpTxd(int Socket,const uchar * TxData,ushort TxLen);
int   ExWNetTcpRxd(int Socket,uchar * RxData,ushort ExpLen,
				   ushort * RxLen,ushort ms);
int   ExWNetClose();
int   ExWNetPPPCheck();

//================================================
//	     TCP/IP functions
//================================================
int NetSocket(int domain, int type, int protocol);
int NetBind(int socket, struct net_sockaddr *addr, socklen_t addrlen);
int NetConnect(int socket, struct net_sockaddr *addr, socklen_t addrlen);
int NetListen(int socket, int backlog);
int NetAccept(int socket, struct net_sockaddr *addr, socklen_t *addrlen);
int NetSend(int socket, void *buf, int size, int flags);
int NetSendto(int socket, void *buf, int size, int flags, 
		struct net_sockaddr *addr, socklen_t addrlen);
int NetRecv(int socket, void *buf, int size, int flags);
int NetRecvfrom(int socket, void *buf, int size, int flags, 
		struct net_sockaddr *addr, socklen_t *addrlen);
int NetCloseSocket(int socket);
int Netioctl(int socket, int cmd, int arg);
int SockAddrSet(struct net_sockaddr *addr, char *ip_str, short port);
int SockAddrGet(struct net_sockaddr *addr, char *ip_str, short *port);
int PPPLogin(char *name, char *passwd, long auth , int timeout);
void PPPLogout(void);
int PPPCheck(void);
int EthSet(
		char *host_ip, char *host_mask,
		char *gw_ip,
		char *dns_ip
		);
int EthGet(
		char *host_ip, char *host_mask,
		char *gw_ip,
		char *dns_ip,
		long *state
		);
int DnsResolve(char *name, char *result, int len);
int NetPing(char *dst_ip_str, long timeout, int size);
int RouteSetDefault(int ifIndex);
int RouteGetDefault(void);
int DhcpStart(void);
void DhcpStop(void);
int DhcpCheck(void);


//================================================
//	     USB functions
//================================================
int UsbDevStart(void);
int UsbDevStop(void);
int UsbDevCheck(void);
int UsbDevSend(char *buf, int size, int timeout_ms);
int UsbDevRecv(char *buf, int size, int timeout_ms);


#ifdef __cplusplus
};
#endif

#endif  // ifdef _POS_S80


#endif	// #ifndef  _POSAPI_S80_H_
