#ifndef POSAPI_CS60__H
#define POSAPI_CS60__H

#include "sslapi.h"

#define uint unsigned int
#define uchar unsigned char

#define MAX_RSA_MODULUS_BITS		2048 
#define MAX_RSA_MODULUS_LEN		((MAX_RSA_MODULUS_BITS + 7) / 8)
#define MAX_RSA_PRIME_BITS		((MAX_RSA_MODULUS_BITS + 1) / 2)
#define MAX_RSA_PRIME_LEN		((MAX_RSA_PRIME_BITS + 7) / 8)


typedef struct {
	unsigned char AppName[32];
	unsigned char AID[16];
	unsigned char AppVer[16];
	unsigned char AppProvider[32];
	unsigned char Descript[64];
	unsigned char DownloadTime[14];
	unsigned long MainAddress;
	unsigned long EventAddress;
	unsigned char AppNum;
	unsigned char RFU[73];
}APPINFO;

//APP BIN INFO
typedef struct
{
	uint uiMainEntry;
	uint uiEventMainEntry;
	uint uiAppInfoEntry;
	
	uint uiBinStart;
	uint uiBinEnd;
	
	uint uiTextStart;
	uint uiTextEnd;//��rodata
	uint uiTextPhyStart;
	uint uiTextPhyEnd;
	
	uint uiDataStart;
	uint uiDataEnd;//=_edata;
	uint uiDataPhyStart;
	uint uiDataPhyEnd;//=_edata;
	uint uiBssStart;
	uint uiBssEnd;
	uint uiSigInfoLen;
	uint uiAppStack;
	uchar aucDllVer; //the posapi.a version
	uchar aucRFU[179];
	uchar aucBinInfoTag[8];
}APP_BIN_INFO;

typedef struct{
	unsigned char RetCode;
	unsigned char track1[256];
	unsigned char track2[256];
	unsigned char track3[256];
}ST_MAGCARD;

typedef struct{
	int MsgType;		    //MAGCARD_MSG,ICCARD_MSG,KEYBOARD_MSG,USER_MSG
	ST_MAGCARD MagMsg;      //MAGCARD_MSG
	unsigned char KeyValue;  //ICCARD_MSG
	unsigned char IccSlot;   //KEYBOARD_MSG
	void *UserMsg;           //USER_MSG
}ST_EVENT_MSG;

typedef struct
{
    unsigned char       fid;
    unsigned char       attr;
    unsigned char       type;
    char        		name[17];
    unsigned long       length;
} FILE_INFO;

typedef struct {
	unsigned int bits;                     /* length in bits of modulus */
	unsigned char modulus[MAX_RSA_MODULUS_LEN];  /* modulus */
	unsigned char exponent[4]; /* public exponent */
} R_RSA_PUBLIC_KEY;


typedef struct {
	unsigned int bits;							/* length in bits of modulus */
	unsigned char modulus[MAX_RSA_MODULUS_LEN];			/* modulus */
	unsigned char publicExponent[4];	/* public exponent */  
	unsigned char exponent[MAX_RSA_MODULUS_LEN];		/* private exponent */
	unsigned char prime[2][MAX_RSA_PRIME_LEN];			/* prime factors */
	unsigned char primeExponent[2][MAX_RSA_PRIME_LEN];	/* exponents for CRT */
	unsigned char coefficient[MAX_RSA_PRIME_LEN];		/* CRT coefficient */
} R_RSA_PRIVATE_KEY;


#define MCC_LEN     3
#define MNC_LEN		3
#define LAC_LEN		6
#define CELL_LEN	9
#define BSIC_LEN    5
#define CHANN_LEN	5
#define RXLEV_LEN	5

#define MAX_CELL_NUM    7   //the max cell number

typedef struct WlGSMCellInfo
{
	char mcc[MCC_LEN+1];//mobile country code 16����ASC���ַ���
	char mnc[MNC_LEN+1];//moblie network code 16����ASC���ַ���
	char lac[LAC_LEN+1];//location code 10����ASC���ַ���
	char cell[CELL_LEN+1];//cell code 10����ASC���ַ���
	char bsic[BSIC_LEN+1];//base station identifier code 10����ASC���ַ���
	char chann[CHANN_LEN+1];//absolute Frequency channel number 10����ASC���ַ���
	char rxlev[RXLEV_LEN+1];//receive level 10����ASC���ַ���
	char reserver[33]; //resever for future use.
}WlGSMCellInfo_T;

typedef struct WlLbs
{
	WlGSMCellInfo_T gsm[MAX_CELL_NUM];
}WlLbs_T;

#define NET_AF_INET 1
	
#define NET_OK    0      /* No error, everything OK. */
#define NET_ERR_MEM  -1      /* Out of memory error.     */
#define NET_ERR_BUF  -2      /* Buffer error.            */
	
#define NET_ERR_ABRT -3      /* Connection aborted.      */
#define NET_ERR_RST  -4      /* Connection reset.        */
#define NET_ERR_CLSD -5      /* Connection closed.       */
#define NET_ERR_CONN -6      /* Not connected.           */
	
#define NET_ERR_VAL  -7      /* Illegal value.           */
	
#define NET_ERR_ARG  -8      /* Illegal argument.        */
	
#define NET_ERR_RTE  -9      /* Routing problem.         */
	
#define NET_ERR_USE  -10     /* Address in use.          */
	
#define NET_ERR_IF   -11     /* Low-level netif error    */
#define NET_ERR_ISCONN -12   /* Already connected.       */
	
#define NET_ERR_TIMEOUT -13  /* time out */
	
#define NET_ERR_AGAIN  -14   /*no block, try again */
#define NET_ERR_EXIST  -15   /* exist already */
#define NET_ERR_SYS    -16   /* sys don not support */
	
	/* PPP ERROR code */
#define NET_ERR_PASSWD -17 /* error password */
#define NET_ERR_MODEM  -18 /* modem dial is fail */
#define NET_ERR_LINKDOWN -19/* DataLink Down */
#define NET_ERR_LOGOUT   -20/* User Logout */
#define NET_ERR_PPP      -21/* PPP Link Down*/
	
#define NET_ERR_STR     -22 /* String Too Long, Illeage */
#define NET_ERR_DNS     -23 /* DNS Failure: No such Name */
#define NET_ERR_INIT    -24 /* No Init */
#define NET_ERR_NEED_START_DHCP    -25 /*δ����DHCP*/
	
#define MAX_IPV4_ADDR_STR_LEN            15
	
#define PB_NETSOCKETBUFF (4096)
	
enum
{
	NET_SOCK_STREAM=1,/* TCP */
	NET_SOCK_DGRAM,/* UDP */
};

struct net_sockaddr
{
	char sa_len;
	char sa_family;
	char sa_data[14];
};

struct net_in_addr 
{
	unsigned long s_addr;
};

struct net_sockaddr_in 
{
		 char  sin_len;
		 char  sin_family;
		 short sin_port;
		 struct net_in_addr sin_addr;
		 char sin_zero[8];
};

#define APPINFO_SECTION __attribute__((used, section(".appinfo"), \
	aligned(sizeof(long))))


void SysBatteryHandlerSet(void(*TimerFunc)(void));
void SysNetHandlerSet(void(*TimerFunc)(void));
void SysLedHandlerSet(void(*TimerFunc)(void));
int WlGetSignal(unsigned char* pSignalLevel);
extern void s_systemInit(void);
extern int  SystemInit(void); 
extern int  SetTime(unsigned char *time);
extern void GetTime(unsigned char *time); 
extern void DelayMs(int Ms); 
extern void TimerSet(unsigned int TimerNo,unsigned int Cnts);
extern int TimerCheck(unsigned int TimerNo);
extern unsigned int GetTimerCount(void); 
extern void ReadSN(unsigned char *SerialNo); 
extern int ReadCSN(unsigned char *CSN, int BuferLen); 
extern void ReadVerInfo(unsigned char *VerInfo); 
extern int GetTermInfo(unsigned char *out_info);
extern void Reboot(void);
extern void GetRandom(unsigned char *random);
extern void SysIdle(void); 
extern int BatteryCheck(void); 
extern int GetEnv(char *name, unsigned char *value); 
extern int PutEnv(char *name, unsigned char *value); 
extern int SysConfig(uchar *ConfigInfoIn, int InfoInLen);
extern int GetTermInfoExt(uchar *InfoOut, int MaxBufLen);
extern void PowerOff(void);
extern void *MemMalloc(int size); 
extern void MemFree(void *pt);
extern int LogPrint(unsigned char *str, unsigned short strlen);
extern void EXReadSN(uchar *SN);
extern int LedLight(uchar ucLedIndex,uchar ucOnOff);
extern int AppFlashEraseItem (uint id);
extern int AppFlashWriteItem (uint id,uint uiLen, uchar* pucDataIn);
extern int AppFlashReadItem(uint id,uint uiLen,uchar *pucDataOut);
extern int AppFlashGetItemSize(uint id);
extern int UniqueKeyRead(unsigned char *key,int len);

extern void Des(unsigned char *input,unsigned char *output,unsigned char *key, int mode);
extern int RSARecover(unsigned char *pbyModule, unsigned int dwModuleLen, 	unsigned char *pbyExp, 
                unsigned int dwExpLen, unsigned char *pbyDataIn, unsigned char *pbyDataOut);
extern int SHA( int Mode,const unsigned char *DataIn,unsigned int DataInLen,unsigned char *ShaOut);
extern int AES(const unsigned char *Input, unsigned char *Output, const unsigned char *AesKey, int KeyLen, int Mode);
extern int ACBC(unsigned    char *iv, const unsigned char *in, unsigned char *out, int dataLen, unsigned char *key, int keyLen, unsigned char mode);
extern int MD5( unsigned char *input, int ilen, unsigned char output[16]);
extern int RSASign(R_RSA_PRIVATE_KEY *privateKey, unsigned char *data_in, unsigned int in_len, 
                unsigned char *data_sign, unsigned int *sign_len);
extern int RSAVerify(R_RSA_PUBLIC_KEY *publicKey, unsigned char *data_in, unsigned int in_len,
                unsigned char *data_sign, unsigned int sign_len); 
extern int Hmac( const unsigned char *input, int ilen, unsigned char output[32], 
                const unsigned char *key, int keylen,  int mode );
				
extern int GetKey();
extern void LedDisplay(int type,unsigned char *str);

extern int FileOpen(char *filename,int Mode);
extern int FileClose(int fid);
extern int FileRead(int fid, int offset, unsigned char *dat, int len);
extern int FileWrite(int fid, int offset, unsigned char *dat, int datlen);
extern int FileSize(char *filename);
extern int FileDel(char *fname);
extern int GetFileInfo(FILE_INFO* finfo);
extern int FileToApp(uchar *FileName);
extern int FileToOs(uchar *FileName);				
extern int FileFreeSize(void);

extern int PortOpen(int channel, uchar *Attr);
extern int PortClose(int channel);
extern int PortSends(int channel, uchar *str, int strlen);
extern int PortRecvs(int channel,uchar *str, int strlen, uint ms);
extern int PortReset(int channel);

// TCP
extern int PB_NetCreate(int domain,int type,int protocol);
extern int PB_NetConnect(int socket,char* ip, unsigned short port);
extern int PB_Connected(int socket);
extern int PB_NetClose (int socket);
extern int PB_NetSend(int socket, void *buf, int size);
extern int PB_NetRecv(int socket, void *buf, int size,unsigned int timeout);
extern int PB_DnsResolve(char *name, char *result, int len);
extern int PB_NetCheck();
extern int PB_RouteSetDefault(int ifIndex);
extern int PB_RouteGetDefault(void);
extern int PB_NetDevGet(int Dev,char *HostIp,char *Mask,char *GateWay,char *Dns);

int WlConfig(char *SimPinIn,char *apn,char *user,char *pwd);
int WlGetLBS(WlLbs_T *info);

extern int WifiConfig(char *essid, char *pwd);
extern int WifiSwitchMode (int mode, int onoff);

//extern uchar BtOpen();

extern int SoundPlay(char * param,int type);
extern int SoundStop();
extern int SoundVolumeSet(int Volume, int  type);
extern int SoundVolumeGet(int *Volume,int  type);
extern void SoundQueueHandlerSet(void (*SoundHandler)(void));
extern int SoundPlayCheck(void);



extern int TLSCreate(void);
extern int TLSConnect(int socket,char* ip, unsigned short port);
extern int TLSSend(int socket, char* dat, int datlen);
extern int TLSRecv(int socket, char* dat, int datlen, unsigned int timeout);
extern int TLSDisconnect(int socket);
extern int TLSCertsSet(int socket,SSL_BUF_T *loc_cert,int loc_cert_num,SSL_BUF_T * local_privatekey,SSL_BUF_T *ca,int ca_num);
extern void TLSServerCertAckSet(int (*CallbackServCertAck)(CERT_INVAL_CODE ));

extern int BtOpen(void);
extern int BtClose(void);
extern int BtSend(unsigned char *dat,int datlen);
extern int BtRecv(uchar *dat,int datlen,uint timeout);
extern int BtGetStatus(void);

#define ulong unsigned long
//#define ushort unsigned short


#define     KEY1                0x31
#define     KEY2                0x32
#define     KEY3                0x33
#define     KEY4                0x34
#define     KEY5                0x35
#define     KEY6                0x36
#define     KEY7                0x37
#define     KEY8                0x38
#define     KEY9                0x39
#define     KEY0                0x30

#define     KEYCLEAR            0x08
#define     KEYALPHA            0x07
#define     KEYUP               0x05
#define     KEYDOWN             0x06
#define     KEYFN               0x15
#define     KEYMENU             0x14
#define     KEYENTER            0x0d
#define     KEYCANCEL           0x1b
#define     NOKEY               0xff

//  S80û�еļ�ֵ
#define     KEYF1               0x01
#define     KEYF2               0x02
#define     KEYF3               0x03
#define     KEYF4               0x04
#define     KEYF5               0x09
#define     KEYF6               0x0a
#define     KEY00               0x3a
#define     KEYSETTLE           0x16
#define     KEYVOID             0x17
#define     KEYREPRINT          0x18
#define     KEYPRNDOWN          0x1a
#define     KEYPRNUP            0x19


#define CHARSET_WEST        0x01 
#define CHARSET_TAI         0x02 
#define CHARSET_MID_EUROPE  0x03     
#define CHARSET_VIETNAM     0x04     
#define CHARSET_GREEK       0x05     
#define CHARSET_BALTIC      0x06     
#define CHARSET_TURKEY      0x07     
#define CHARSET_HEBREW      0x08     
#define CHARSET_RUSSIAN     0x09     
#define CHARSET_GB2312      0x0A     
#define CHARSET_GBK         0x0B     
#define CHARSET_GB18030     0x0C     
#define CHARSET_BIG5        0x0D     
#define CHARSET_SHIFT_JIS   0x0E     
#define CHARSET_KOREAN      0x0F     
#define CHARSET_ARABIA      0x10   
#define CHARSET_DIY         0x11

#define     ASCII               0x00
#define     CFONT               0x01


typedef struct{
	int CharSet;
	int Width;
	int Height;
	int Bold;
	int Italic;
}ST_FONT;

#define kbflush_p
#define kbflush
#define kbhit_p
#define kbhit
#define getkey_p
#define getkey
#define ScrDisp
#define ScrPrint
#define SetMenu
#define ScrFontSet
#define ScrCls
#endif

