
#ifndef _PAAT_H_
#define _PAAT_H_

//没有出参和入参的函数，以及不需要调用的函数可以不封装

#define TimerSet		paat_TimerSet
#define TimerCheck		paat_TimerCheck
#define GetTimerCount   paat_GetTimerCount

//1键盘			
//回放没有执行操作		getkey GetString GetHzString
#define getkey			paat_getkey
#define kbhit			paat_kbhit
#define GetString		paat_GetString
#define GetHzString		paat_GetHzString
#define KbLock			paat_KbLock
#define KbCheck			paat_KbCheck
//一下函数不需要封装，因为回放测试时不会按键；
//void kbmute(uchar flag);//设置按键板在按键时是否发声
//void kbflush(void);//清除当前键盘缓冲区中的所有未读取的按键
//void kblight(uchar mode);//设置按键背光

//2触摸屏
#define TouchScreenRead		paat_TouchScreenRead
#define TouchScreenOpen		paat_TouchScreenOpen
#define TouchScreenAttrSet	paat_TouchScreenAttrSet
//void TouchScreenClose (void);
//void TouchScreenFlush (void);

//3icc 接触式ic卡读卡器
// #define IccInit			paat_IccInit
// #define IccClose		paat_IccClose
// #define IccAutoResp		paat_IccAutoResp
// #define IccIsoCommand	paat_IccIsoCommand
// #define IccDetect		paat_IccDetect

//4mag 磁头阅读器
//回放没有执行操作		MagSwiped
#define MagSwiped		paat_MagSwiped
#define MagRead			paat_MagRead
#define MagGetMagnePrint paat_MagGetMagnePrint

//5picc 射频卡读卡器
#define PiccOpen		paat_PiccOpen
#define PiccSetup		paat_PiccSetup
#define PiccDetect		paat_PiccDetect
#define PiccIsoCommand	paat_PiccIsoCommand
#define PiccRemove		paat_PiccRemove
#define PiccClose		paat_PiccClose
#define M1Authority		paat_M1Authority
#define M1ReadBlock		paat_M1ReadBlock
#define M1WriteBlock	paat_M1WriteBlock
#define M1Operate		paat_M1Operate
#define PiccLight       paat_PiccLight
#define PiccInitFelica	paat_PiccInitFelica
#define PiccCmdExchange	paat_PiccCmdExchange

//6scan 条码模块
#define ScanOpen		paat_ScanOpen
#define ScanRead        paat_ScanRead
#define ScanClose		paat_ScanClose

//7ped  PED加解密操作
#define PedWriteKey			paat_PedWriteKey
#define PedWriteTIK			paat_PedWriteTIK
#define PedGetPinBlock		paat_PedGetPinBlock
#define PedGetMac			paat_PedGetMac
#define	PedCalcDES			paat_PedCalcDES
#define PedGetPinDukpt		paat_PedGetPinDukpt
#define PedGetMacDukpt		paat_PedGetMacDukpt
#define PedReGenPinBlock	paat_PedReGenPinBlock
#define PedVerifyPlainPin	paat_PedVerifyPlainPin
#define PedVerifyCipherPin	paat_PedVerifyCipherPin
#define PedGetKcv			paat_PedGetKcv
#define PedWriteKeyVar		paat_PedWriteKeyVar
#define PedGetVer			paat_PedGetVer

#define PedErase			paat_PedErase
#define PedSetIntervaltime	paat_PedSetIntervaltime
#define PedSetKeyTag		paat_PedSetKeyTag
#define PedSetFunctionKey	paat_PedSetFunctionKey
#define PedWriteRsaKey		paat_PedWriteRsaKey
#define PedRsaRecover		paat_PedRsaRecover
//#define PedDukptDes			paat_PedDukptDes
#define PedGetDukptKSN		paat_PedGetDukptKSN
#define PedDukptIncreaseKsn paat_PedDukptIncreaseKsn
#define	PedGetPinRsa		paat_PedGetPinRsa
#define PedReadRsaKey		paat_PedReadRsaKey
#define PedWriteAesKey		paat_PedWriteAesKey
#define PedCalcAes			paat_PedCalcAes
#define PedGenSM2KeyPair	paat_PedGenSM2KeyPair
#define PedWriteSM2Key		paat_PedWriteSM2Key
#define PedSM2Sign			paat_PedSM2Sign
#define PedSM2Verify		paat_PedSM2Verify
#define PedSM2Recover		paat_PedSM2Recover
#define PedSM3				paat_PedSM3
#define PedSM4				paat_PedSM4
#define PedGetMacSM			paat_PedGetMacSM
#define PedGetPinBlockSM4	paat_PedGetPinBlockSM4
#define PedWriteKeyEncByRsa	paat_PedWriteKeyEncByRsa
#define PedSetOfflinePinMode	paat_PedSetOfflinePinMode

//8print 打印相关函数
#define PrnInit			paat_PrnInit
#define PrnSelectFont	paat_PrnSelectFont
#define PrnFontSet		paat_PrnFontSet
#define PrnSpaceSet		paat_PrnSpaceSet
#define PrnStep			paat_PrnStep
//#define PrnStr			paat_PrnStr
//#define PrnLogo			paat_PrnLogo
#define PrnStart		paat_PrnStart
#define PrnStatus		paat_PrnStatus
#define PrnLeftIndent	paat_PrnLeftIndent
#define PrnGetDotLine	paat_PrnGetDotLine
#define PrnSetGray		paat_PrnSetGray
#define PrnGetFontDot	paat_PrnGetFontDot
#define PrnDoubleWidth	paat_PrnDoubleWidth
#define PrnDoubleHeight	paat_PrnDoubleHeight
#define PrnAttrSet		paat_PrnAttrSet
//#define PrnConfig		paat_PrnConfig
#define PrnImage		paat_PrnImage
#define PrnPreFeedSet   paat_PrnPreFeedSet

//9Scr	屏显相关函数
#if 0
#define ScrCls 			paat_ScrCls     //清屏
#define ScrClrLine		paat_ScrClrLine //清行
#define ScrGray			paat_ScrGray	//设置对比度、彩屏表示设置亮度
#define ScrBackLight	paat_ScrBackLight	//设置屏幕背光
#define ScrGotoxy		paat_ScrGotoxy	//移动屏幕光标
#define ScrSelectFont	paat_ScrSelectFont	//设置字体   可设置多种
#define ScrFontSet		paat_ScrFontSet	//设置字体 ASCII 6*8; CFONT 16*16 or  8*16
#define ScrAttrSet		paat_ScrAttrSet	//正反显示
#define Lcdprintf    	paat_Lcdprintf	//先调用FontSet
#define ScrPrint		paat_ScrPrint	//格式化显示
#define ScrPlot			paat_ScrPlot	//屏幕指定位置画点
#define ScrDrLogo		paat_ScrDrLogo	//显示logo点阵
#define ScrRestore		paat_ScrRestore	//保存或恢复屏显内容
#define ScrSetIcon		paat_ScrSetIcon	//显示指定图标
#define ScrGotoxyEx		paat_ScrGotoxyEx	//定位屏显光标
#define ScrGetxyEx		paat_ScrGetxyEx	//读取光标位置
#define ScrDrawRect		paat_ScrDrawRect	//画矩形
#define ScrClrRect		paat_ScrClrRect	//清楚矩形区域屏显信息
#define ScrSpaceSet		paat_ScrSpaceSet	//设置显示的行间距或字间距
#define ScrGetLcdSize	paat_ScrGetLcdSize	//获取显示区域
#define ScrTextOut		paat_ScrTextOut	//指定的位置显示字符串
#define ScrSetOutput	paat_ScrSetOutput	//选择输出屏幕或者显存  仅对S60适用
#define ScrSetEcho		paat_ScrSetEcho	//显示回显 仅对S60适用

//彩屏
#define CLcdGetInfo		paat_CLcdGetInfo	//获取彩屏信息
#define CLcdSetFgColor 	paat_CLcdSetFgColor //设置前景文字的颜色
#define CLcdSetBgColor 	paat_CLcdSetBgColor	//设置背景颜色
#define CLcdBgDrawImg	paat_CLcdBgDrawImg	//在屏幕指定位置显示图片
#define CLcdBgDrawBox	paat_CLcdBgDrawBox	//背景画实心矩形框
#define CLcdDrawPixel	paat_CLcdDrawPixel	//画点
#define CLcdGetPixel	paat_CLcdGetPixel	//获取颜色
#define CLcdDrawRect	paat_CLcdDrawRect	//屏幕前景画矩形
#define CLcdClrRect		paat_CLcdClrRect	//清楚前景矩形
#define CLcdTextOut		paat_CLcdTextOut	//前景显示
#define CLcdPrint		paat_CLcdPrint		//
#define CLcdClrLine		paat_CLcdClrLine
#define CLcdBgDrawGif	paat_CLcdBgDrawGif	//GIF
#define CLcdBgClrGif	paat_CLcdBgClrGif
#endif

//10UART	串口通信

//11USB		USB连接

//12eth 网络模块
//网络协议栈
#define NetSocket		paat_NetSocket
#define NetBind			paat_NetBind
#define NetConnect		paat_NetConnect
#define NetListen		paat_NetListen
#define NetAccept		paat_NetAccept
#define NetSend			paat_NetSend
#define NetSendto		paat_NetSendto
#define NetRecv			paat_NetRecv  
#define NetRecvfrom		paat_NetRecvfrom
#define NetCloseSocket	paat_NetCloseSocket
#define Netioctl		paat_Netioctl
#define SockAddrSet		paat_SockAddrSet
#define SockAddrGet		paat_SockAddrGet
#define DnsResolve		paat_DnsResolve
#define DnsResolveExt	paat_DnsResolveExt
#define NetPing			paat_NetPing
#define RouteSetDefault	paat_RouteSetDefault
#define RouteGetDefault	paat_RouteGetDefault
#define NetDevGet		paat_NetDevGet
//网络模块
#define EthSet				paat_EthSet
#define EthGet				paat_EthGet
#define DhcpStart			paat_DhcpStart
#define DhcpStop			paat_DhcpStop
#define DhcpCheck			paat_DhcpCheck
#define PppoeLogin				paat_PppoeLogin
#define NetAddStaticArp			paat_NetAddStaticArp
#define EthSetRateDuplexMode	paat_EthSetRateDuplexMode
#define EthGetFirstRouteMac		paat_EthGetFirstRouteMac
#define EthMacGet				paat_EthMacGet

//13wl 无线模块 
#define WlInit				paat_WlInit	
#define WlGetSignal			paat_WlGetSignal
#define WlPppLogin			paat_WlPppLogin
#define WlPppLogout			paat_WlPppLogout
#define WlPppCheck			paat_WlPppCheck
#define WlOpenPort			paat_WlOpenPort
#define WlClosePort			paat_WlClosePort
#define WlSendCmd			paat_WlSendCmd
#define WlSwitchPower		paat_WlSwitchPower
#define WlSelSim			paat_WlSelSim
#define WlAutoStart			paat_WlAutoStart
#define WlAutoCheck			paat_WlAutoCheck
#define WlPppLoginEx		paat_WlPppLoginEx
#define WlTcpRetryNum		paat_WlTcpRetryNum		 
#define WlSetTcpDetect		paat_WlSetTcpDetect
#define WlSetDns			paat_WlSetDns

//14modem 
#define ModemReset		paat_ModemReset
#define ModemDial		paat_ModemDial
#define ModemCheck		paat_ModemCheck
#define ModemTxd		paat_ModemTxd
#define ModemRxd		paat_ModemRxd
#define ModemAsyncGet	paat_ModemAsyncGet
#define OnHook			paat_OnHook
#define HangOff			paat_HangOff
#define ModemExCommand	paat_ModemExCommand
#define PPPLogin		paat_PPPLogin
#define PPPLogout		paat_PPPLogout
#define PPPCheck		paat_PPPCheck

//15wifi
#define WifiOpen		paat_WifiOpen
#define WifiClose		paat_WifiClose
#define WifiScan		paat_WifiScan
#define WifiConnect		paat_WifiConnect
#define WifiDisconnect	paat_WifiDisconnect
#define WifiCheck		paat_WifiCheck
#define WifiCtrl		paat_WifiCtrl
#define WifiScanEx		paat_WifiScanEx

//16bt
#define BtOpen			paat_BtOpen
#define BtClose			paat_BtClose
#define BtGetConfig		paat_BtGetConfig
#define BtSetConfig		paat_BtSetConfig
#define BtScan			paat_BtScan
#define BtConnect		paat_BtConnect
#define	BtDisconnect	paat_BtDisconnect
#define BtGetStatus		paat_BtGetStatus
#define BtCtrl			paat_BtCtrl

/////////////////////////////////////////////////////////////////////////控制操作台
//返回码列表
enum PAAT_RET
{
    //PAAT_RET_OK					=(0),
    //PAAT_ERR_BASE				=(-10001),
	PAAT_ERR_OTHER              =(-10000),
    PAAT_ERR_INVALID_PARAM      =(-10002),
    PAAT_ERR_COMM_RECV          =(-10003),

	PAAT_ERR_IN_PARAM			=(-10004),
	PAAT_ERR_SETCASE1			=(-10005),
	PAAT_ERR_SETCASE2			=(-10006),
	PAAT_ERR_SETCASE3			=(-10007),
	PAAT_ERR_GETCTRL1			=(-10008),
	PAAT_ERR_GETCTRL2			=(-10009),
	PAAT_ERR_GETCTRL3			=(-10010),
	PAAT_ERR_GETMODULE			=(-10011),
	PAAT_ERR_FIRSTRUN			=(-10012),
	PAAT_ERR_RUNMUCH			=(-10013),
	PAAT_ERR_CASEMUCH			=(-10011),
	PAAT_ERR_NOTEXIST			=(-10012),
	PAAT_ERR_AUTOERR			=(-10013)
};

//可能需要录制回放的模块
#define    KEY_MODULE       "KEYBOARD"
#define    TOUCH_MODULE     "TOUCHSCREEN"
#define    MAG_MODULE       "MAG"
#define    IC_MODULE        "ICC"
#define    PICC_MODULE      "PICC"
#define    SCAN_MODULE      "SCAN"
#define    PED_MODULE       "PED"
#define    PRINT_MODULE     "PRINT"
#define    SCREEN_MODULE    "SCREEN"
#define    UART_MODULE      "UART"
#define    USB_MODULE       "USB"
#define    ETH_MODULE       "ETH"
#define    WL_MODULE        "WL"
#define    MODEM_MODULE     "MODEM"
#define    WIFI_MODULE      "WIFI"
#define    BT_MODULE        "BT"


#ifdef __cplusplus
extern "C"
{
#endif

////控制台
int AutoTestCtrl(char *pszCaseFrom, int iCaseNo,int iCtrlFlag);

//设置和获取指定数据
enum PAAT_DATA
{
	//一下四类用于交易产生的数据
	PAAT_FLOW_NUM=0,//交易流水号6字节长度
	PAAT_BATCH_NUM=1,//批次号6字节长度
	PAAT_SALE_TRANS_DATA=2,//交易日期4字节长度
	PAAT_SALE_AUTH_CODE=3,//预授权号6字节长度
	//以下三类用于预授权产生的数据
	PAAT_PRE_AUTH_DATA=4,//预授权日期 4字节
	PAAT_AUTH_CODE=5,//预授权授权码 6字节
	PAAT_PRE_AUTH_COMPLETE_FLOW_NUM=6,//预授权完成流水号6字节
	//用于配置打印一笔交易的流水号
	PAAT_FLOW_NUM_PRINT=7,//根据录制的流水号打印一笔小票6字节长度
	//获取交易密码
	PAAT_PIN_BLOCK=8,	//交易密码，这个只从终端获取值，内部使用，应用不要关心8字节长度
	PAAT_FLOW_COUNT//类型计数，应用请不要使用
};
//回去交易流水号，用于撤单
int paat_GetValue(enum PAAT_DATA nType, char* pData, int nLen);
//设置交易流水，用于交易撤单时使用
int paat_SetValue(enum PAAT_DATA nType, char* pData, int nLen);
//0表示停止录制/回放的脚本操作，直接调用API，1表示恢复录制/回放的脚本操作
void paat_RunStatus(int nStatus);


void paat_TimerSet(unsigned char TimerNo, unsigned short Cnts);
unsigned short paat_TimerCheck(unsigned char TimerNo);
unsigned int paat_GetTimerCount(void);

////////////////////////////////////////////////////////////////////////模块具体定义
//1键盘模块 Keyboard
unsigned char paat_getkey(void);
unsigned char paat_kbhit(void); 
void paat_KbLock(uchar mode);
int paat_KbCheck(int iCmd);
unsigned char paat_GetString(unsigned char *str,unsigned char mode,unsigned char minlen,unsigned char maxlen);
unsigned char paat_GetHzString(unsigned char *outstr, unsigned char max, unsigned short TimeOut);

//2触摸屏 TouchScreen
int paat_TouchScreenOpen (unsigned int mode);
void paat_TouchScreenAttrSet (TS_ATTR_T * pstTsAttr);
int paat_TouchScreenRead(TS_POINT_T *pstTsPoint, unsigned int num);

// 3接触式IC ICC 
// unsigned char paat_IccInit(unsigned char slot,unsigned char *ATR);
// void paat_IccClose(uchar slot);
// void paat_IccAutoResp(uchar slot, uchar autoresp);
// unsigned char paat_IccIsoCommand(unsigned char slot, APDU_SEND *ApduSend, APDU_RESP *ApduRecv);
// unsigned char paat_IccDetect(unsigned char slot);

//4磁头阅读器 MAG
unsigned char paat_MagSwiped(void);
unsigned char paat_MagRead(unsigned char *Track1, unsigned char *Track2, unsigned char *Track3);
uchar paat_MagGetMagnePrint(uchar *pucMagnePrint);//()

//5射频卡读卡器 PICC
uchar paat_PiccOpen(void);
uchar paat_PiccSetup (uchar mode, PICC_PARA *picc_para);
unsigned char paat_PiccDetect(uchar Mode,uchar *CardType,uchar *SerialInfo,uchar *CID,uchar *Other);
unsigned char paat_PiccIsoCommand(uchar cid,APDU_SEND *ApduSend,APDU_RESP *ApduRecv);
uchar paat_PiccRemove(uchar mode,uchar cid);
void paat_PiccClose(void);
uchar paat_M1Authority(uchar Type,uchar BlkNo,uchar *Pwd,uchar *SerialNo);
unsigned char paat_M1ReadBlock(uchar BlkNo,uchar *BlkValue);
unsigned char paat_M1WriteBlock(uchar BlkNo,uchar *BlkValue);
unsigned char paat_M1Operate(uchar Type,uchar BlkNo,uchar *Value,uchar UpdateBlkNo);
void paat_PiccLight(uchar ucLedIndex,uchar ucOnOff); 
uchar paat_PiccInitFelica(uchar ucRate, uchar ucPol);
uchar paat_PiccCmdExchange(uint uiSendLen, uchar* paucInData, uint* puiRecLen, uchar* paucOutData);

//6条码模块
int paat_ScanOpen (void);
int paat_ScanRead(uchar *buf, uint size);
void paat_ScanClose(void);

//7PED模块
int paat_PedWriteKey(ST_KEY_INFO * KeyInfoIn,ST_KCV_INFO * KcvInfoIn);
int paat_PedWriteTIK(uchar GroupIdx,uchar SrcKeyIdx,uchar KeyLen,uchar * KeyValueIn,uchar * KsnIn, ST_KCV_INFO * KcvInfoIn);
int paat_PedGetPinBlock(uchar KeyIdx, uchar *ExpPinLenIn, uchar * DataIn, uchar *PinBlockOut, uchar Mode, ulong TimeoutMs);
int paat_PedGetMac(uchar KeyIdx, uchar *DataIn, ushort DataInLen, uchar *MacOut, uchar Mode);
int paat_PedCalcDES(uchar KeyIdx,uchar * DataIn,ushort DataInLen,uchar * DataOut, uchar Mode);
int paat_PedGetPinDukpt(uchar GroupIdx, uchar *ExpPinLenIn, uchar * DataIn, uchar* KsnOut, uchar * PinBlockOut, uchar Mode, ulong TimeoutMs);
int paat_PedGetMacDukpt(uchar GroupIdx,uchar *DataIn, ushort DataInLen, uchar *MacOut, uchar * KsnOut,   uchar Mode);
int paat_PedReGenPinBlock(uchar UpdateFlag,ST_KEY_INFO *KeyInfoIn, ST_KCV_INFO *KcvInfoIn,uchar *DataIn,uchar *PinBlockOut,uchar Mode);
int paat_PedVerifyPlainPin(uchar IccSlot, uchar *ExpPinLenIn, uchar *IccRespOut,uchar Mode,ulong TimeoutMs);
int paat_PedVerifyCipherPin(uchar IccSlot, uchar *ExpPinLenIn, RSA_PINKEY *RsaPinKeyIn, uchar *IccRespOut, uchar Mode,ulong TimeoutMs);
int paat_PedGetKcv(uchar KeyType,uchar KeyIdx,ST_KCV_INFO *KcvInfoOut);
int paat_PedWriteKeyVar (uchar KeyType,uchar SrcKeyIdx,uchar DstKeyIdx, uchar * XorVarIn,ST_KCV_INFO * KcvInfoIn);
int paat_PedGetVer(uchar * VerInfoOut);

int paat_PedErase();
int paat_PedSetIntervaltime(ulong TPKIntervalTimeMs, ulong TAKIntervalTimeMs);
int paat_PedSetKeyTag(uchar * KeyTagIn);
int paat_PedSetFunctionKey(uchar ucKey);
int paat_PedWriteRsaKey(uchar RSAKeyIndex, ST_RSA_KEY* pstRsakeyIn);
int paat_PedRsaRecover (uchar RSAKeyIndex, uchar *pucDataIn, uchar * pucDataOut,uchar* pucKeyInfoOut);
int paat_PedDukptDes(uchar GroupIdx, uchar KeyVarType,uchar * pucIV, ushort DataInLen, uchar *DataIn, uchar * DataOut, uchar*KsnOut ,uchar Mode);
int paat_PedGetDukptKSN(uchar GroupIdx, uchar * KsnOut);
int paat_PedDukptIncreaseKsn(uchar GroupIdx);
int paat_PedGetPinRsa(uchar RsaKeyIdx,uchar *ExpPinLenIn, uchar * DataIn, uchar *PinBlockOut,uchar Mode, ulong TimeOutMs);
int paat_PedReadRsaKey(uchar RSAKeyIndex, ST_RSA_KEY* pstRsakeyOut);
int paat_PedWriteAesKey(ST_AES_KEY_INFO * AesKeyInfoIn, ST_KCV_INFO * KcvInfoIn);
int paat_PedCalcAes(unsigned char KeyIdx, unsigned char * InitVector, const unsigned char *DataIn, unsigned short DataInLen, unsigned char *DataOut,unsigned char Mode);
int paat_PedGenSM2KeyPair(uchar *PvtKey,uchar *PubKey,ushort KeyLenBit);
int paat_PedWriteSM2Key(uchar KeyIdx,uchar KeyType, uchar *KeyValue);

int paat_PedSM2Sign(uchar PubKeyIdx,uchar PvtKeyIdx,uchar *Uid,ushort UidLen,uchar *Input,uint InputLen,uchar *Signature);
int paat_PedSM2Verify(uchar PubKeyIdx,uchar *Uid,ushort UidLen,uchar *Input,uint InputLen,const uchar *Signature); 
int paat_PedSM2Recover (uchar KeyIdx,uchar *Input,ushort InputLen, uchar *Output,ushort *OutputLen,uchar Mode);
int paat_PedSM3(uchar *Input,uint InputLen,uchar *Output,uchar Mode);
int paat_PedSM4(uchar KeyIdx,uchar *Initvector,uchar *Input,ushort InputLen, uchar *Output,uchar Mode);
int paat_PedGetMacSM (uchar KeyIdx,uchar *InitVector,uchar *Input,ushort InputLen,uchar *MacOut,uchar Mode);
int paat_PedGetPinBlockSM4(uchar KeyIdx,uchar *ExpPinLenIn, uchar * DataIn,uchar *PinBlockOut,uchar Mode, ulong TimeOutMs);
int paat_PedWriteKeyEncByRsa (const uchar *DataIn,ushort DataInLen,uchar DataInFormat,uchar PrvKeyIdx,uchar DstKeyType,uchar DstKeyIdx,const uchar *KsnIn,ST_KCV_INFO * KcvInfoIn);
int paat_PedSetOfflinePinMode(uchar Mode,uchar TpkIdx,uchar *PinBlock,ushort PinBlockLen);

//12网络模块 ETH
int paat_NetSocket(int domain,int type,int protocol);
int paat_NetBind(int socket,struct net_sockaddr *addr, socklen_t addrlen);
int paat_NetConnect(int socket,struct net_sockaddr *addr,socklen_t addrlen);
int paat_NetListen(int socket, int backlog);
int paat_NetAccept(int socket, struct net_sockaddr *addr, socklen_t *addrlen);
int paat_NetSend(int socket,void *buf,int size,int flags);
int paat_NetSendto(int socket,void *buf,int size,int flags,struct net_sockaddr* addr,socklen_t addrlen);
int paat_NetRecv(int socket,void *buf,int size,int flags);
int paat_NetRecvfrom(int socket,void *buf,int size,int flags,struct net_sockaddr* addr,socklen_t* addrlen);
int paat_NetCloseSocket(int socket);
int paat_Netioctl(int socket,int cmd,int arg);
int	paat_SockAddrSet(struct net_sockaddr* addr,char* ip_str, short port);
int paat_SockAddrGet(struct net_sockaddr* addr,char* ip_str, short* port);
int paat_DnsResolve(char* name,char* result,int len);
int paat_DnsResolveExt(char *name, char *result, int len, int timeout);
int paat_NetPing(char *dst_ip_str,long timeout,int size);
int paat_RouteSetDefault(int ifIndex);
int paat_RouteGetDefault();
int paat_NetDevGet(int Dev,char* HostIp,char* Mask,char* GateWay,char* Dns);
int paat_EthSet(char* host_ip,char* host_mask,char* gw_ip,char* dns_ip);
int paat_EthGet(char* host_ip,char* host_mask,char* gw_ip,char* dns_ip,long* state);
int paat_DhcpStart();
void paat_DhcpStop();
int paat_DhcpCheck();
int paat_PppoeLogin(char *name,char *passwd,int timeout);
int paat_NetAddStaticArp(char *ip_str, unsigned char mac[6]);
void paat_EthSetRateDuplexMode (int mode);
int paat_EthGetFirstRouteMac (const char *dest_ip, unsigned char *mac, int len);
int paat_EthMacGet(unsigned char mac[6]);

//13无线模块 Wl
int paat_WlInit(const uchar *SimPinIn);
int paat_WlGetSignal(uchar * SingnalLevelOut);
int paat_WlPppLogin(uchar *APNIn, uchar * UidIn, uchar * PwdIn,long Auth,int TimeOut,int AliveInterval );
void paat_WlPppLogout (void);
int paat_WlPppCheck(void);
int paat_WlOpenPort ( void );
int paat_WlClosePort ( void );
int paat_WlSendCmd(const uchar * ATstrIn,uchar *RspOut,ushort Rsplen,ushort TimeOut, ushort Mode);
void paat_WlSwitchPower (uchar Onoff );
int paat_WlSelSim (uchar simno );
int paat_WlAutoStart(unsigned char *pin,unsigned char *APNIn, unsigned char *UidIn,unsigned char * PwdIn,  long Auth, int TimeOut,int AliveInterval);
int paat_WlAutoCheck(void);
int paat_WlPppLoginEx(const char *DialNum,const char *apn,char *Uid, char *Pwd, long Auth, int timeout,int AliveInterval);
void paat_WlTcpRetryNum(int value);
void paat_WlSetTcpDetect(int value);
int paat_WlSetDns(char *dns_ip);


//14MODEM WIFI BT 
uchar paat_ModemReset();
uchar paat_ModemDial(COMM_PARA *MPara, uchar *TelNo, uchar mode);
uchar paat_ModemCheck(void);
uchar paat_ModemTxd(uchar *SendData, ushort len);
uchar paat_ModemRxd(uchar *RecvData,ushort *len);
uchar paat_ModemAsyncGet(uchar *ch); 
uchar paat_OnHook(void);
uchar paat_HangOff(void);
ushort paat_ModemExCommand(uchar *CmdStr, uchar *RespData, ushort *Dlen,ushort Timeout10ms); 
int paat_PPPLogin(char *name, char *passwd, long auth ,int timeout);
void paat_PPPLogout(void);
int paat_PPPCheck(void);
//15WIFI
int paat_WifiOpen(void);
int paat_WifiClose(void);
int paat_WifiScan (ST_WIFI_AP *outAps,unsigned int ApCount);
int paat_WifiConnect(ST_WIFI_AP *Ap,ST_WIFI_PARAM *WifiParam);
int paat_WifiDisconnect(void); 
int paat_WifiCheck(ST_WIFI_AP *Ap);
int paat_WifiCtrl(unsigned int iCmd,void *pArgIn,unsigned int iSizeIn,void* pArgOut,unsigned int iSizeOut);
int paat_WifiScanEx(ST_WIFI_AP_EX *outAps, unsigned int apCount );
//16BT
int paat_BtOpen(void);
int paat_BtClose(void); 
int paat_BtGetConfig(ST_BT_CONFIG *pCfg);
int paat_BtSetConfig(ST_BT_CONFIG *pCfg);
int paat_BtScan(ST_BT_SLAVE *pSlave,unsigned int Cnt,unsigned int TimeOut);
int paat_BtConnect(ST_BT_SLAVE *Slave);
int paat_BtDisconnect(void);
int paat_BtGetStatus(ST_BT_STATUS *pStatus);
int paat_BtCtrl(unsigned int iCmd, void *pArgIn, unsigned int iSizeIn, void* pArgOut, unsigned int iSizeOut);

#ifdef __cplusplus
};
#endif

#endif

