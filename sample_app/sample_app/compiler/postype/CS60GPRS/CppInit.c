#define __CPP_CONSTRUCTOR_LIST_START __CTOR_LIST__
#define __CPP_CONSTRUCTOR_LIST_END __CTOR_END__
#define __CPP_DESTRUCTOR_LIST_START __DTOR_LIST__
#define __CPP_DESTRUCTOR_LIST_END __DTOR_END__

extern unsigned int __CPP_CONSTRUCTOR_LIST_START;
extern unsigned int __CPP_CONSTRUCTOR_LIST_END;
extern unsigned int __CPP_DESTRUCTOR_LIST_START;
extern unsigned int __CPP_DESTRUCTOR_LIST_END;
typedef void (*func_ptr)(void);

#ifdef __cplusplus
extern "C" void s_ctors();
extern "C" void s_dtors();
#endif

//调用C++全局对象构造函数
void s_ctors()
{
#ifdef _CPP_CTOR_DTOR_//编译时如果为C++项目，在makefile中定义此宏
	volatile func_ptr *p;
	volatile func_ptr f;
	//构造函数列表起始地址
	unsigned int* pCtorStartValue = (unsigned int *)((unsigned int)(&__CPP_CONSTRUCTOR_LIST_START));
	//构造函数列表结束地址的下一个地址
	unsigned int* pCtorEndValue = (unsigned int *)((unsigned int)(&__CPP_CONSTRUCTOR_LIST_END));

	p = (func_ptr*)pCtorEndValue;
	p--;

	//构造函数逆向调用，即从最后一个开始
	while(((unsigned int)p) >= (unsigned int)pCtorStartValue)
	{
		f = *p;
		if (f != 0)
		{
			f();//调用构造函数
		}
		p--;
	}
#endif
}

//调用C++全局对象析构函数
 void s_dtors()
 {
#ifdef _CPP_CTOR_DTOR_
	volatile func_ptr f;
	 //析构函数列表起始地址
	func_ptr *pStart = (func_ptr *)((int)(&__CPP_DESTRUCTOR_LIST_START));
	func_ptr *pEnd = (func_ptr *)((int)(&__CPP_DESTRUCTOR_LIST_END));

	while(pStart<pEnd)
	{
		f = *pStart;
		if(f != 0)
			f();
		pStart++;
	}
#endif
 }
