#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "cs60_app.h"

#ifndef uint
#define uint unsigned int
#endif
#ifndef uchar
#define uchar unsigned char
#endif

#ifndef ushort
#define ushort unsigned short int
#endif
#define CS60_POS
#include <posapi.h>
#include <posapi_all.h>


//#define PROLIN_OS_APP     //for monitor App on prolin system
#ifndef PROLIN_OS_APP
const APPINFO AppInfo = {
#else
	const APPINFO_SECTION APPINFO AppInfo = {
#endif

	"CS60 StaticQR 1.0",
	"QRBOX_APP",//for Prolin OS,this item can not contain " -+= "(special character)
	"V1.1.1_EN",
	"IOTTeam",
	"Static QR App",
	"",
	0,
	0,
	0,
	""
};

#define LED_RED		0X01
#define LED_GREEN	0X02
#define LED_BLUE	0X04
unsigned char gu8aLedIndex[3] = {LED_RED,LED_GREEN,LED_BLUE};

int value = 0;

int stopCounter = 0;

void Log(char *fmt, ...)
{
	char  buffer[700] = { 0 };
	int ret = 0;

	va_list varg;
	va_start(varg, fmt);
	ret = vsnprintf(buffer, sizeof(buffer), (const char*)fmt, varg);
	va_end(varg);
	PortSends(11, (unsigned char*)buffer, strlen(buffer));
}

void LedSetCallback()
{
	int key =0;
	char str[10];
	
	while(1)
	{
		sprintf(str, "%d", value);
		DelayMs(200);
		key = GetKey();
		//Log("key: %d\r\n",key);
		if(key == 43){   // +
			SoundVolumeSet(2,0);
			//LedDisplay(1,"Welcome");
			//SoundPlay(str,3);
			//SoundStop();
			LedLight(gu8aLedIndex[0],1);
			DelayMs(2000);
			LedLight(gu8aLedIndex[0],0);
			DelayMs(2000);
		}
		else if(key == 45 ){ // -
			LedLight(gu8aLedIndex[1],1);
			DelayMs(2000);
			LedLight(gu8aLedIndex[1],0);
			DelayMs(2000);
			stopCounter = 1;
			
		}
		else if(key == 79 ){  // wifi
			stopCounter =2;	
			LedLight(gu8aLedIndex[2],1);
			DelayMs(2000);
			int y = BatteryCheck();
			
			// char tempCharge[10];
			// sprintf(tempCharge, "%d", y);
			// LedDisplay(tempCharge,1);
			DelayMs(1000);
			if( y == 100) {
				int a1 = SoundPlay("BatPre.wav",1);
				DelayMs(2000);
				int a2 = SoundPlay("FullBatt.wav",1);
				Log("A2 %d \n \n", a2);
			}
			LedLight(gu8aLedIndex[2],0);
			DelayMs(2000);
		}
		else{
			
		}
	}
	SysLedHandlerSet(NULL);		// close callback function
}

/*
	Function for scroling
*/

void scrolling(char floa[]) {
	int i=0;int j=0;
	int k = 0;	
	for(k =0;k < strlen(floa) - 5 ; k++) 
	{
		char set[6];
		for(j=0;j<6;j++)
		{
			set[j]=floa[i+j];
		}
		LedDisplay(1, set);
		DelayMs(600);
		i++;
	}
	for(k=strlen(floa) - 5;k < strlen(floa);k++) {
		char set[6] = "      ";int i=0;
		
		for(j=k;j<strlen(floa);j++) {
			set[i] = floa[j];
			i++;
		}
		LedDisplay(1, set);
		DelayMs(600);
	}
	return 0;
}

void playDigit(int x) {
	char str[10];
	sprintf(str, "%d", x);
	char dest[50];
	if(x <= 20) {
		strcpy(dest, "num");
		strcat(dest, str);
		strcat(dest, ".wav");
		SoundPlay(dest, 1);
	}
	return 0;
}


void convert(char *num) {
   int len = strlen(num);
   // cases
   if (len == 0) {
      Log("%s\n", "empty string\n");
      return;
   }
   if (len > 4) {
      Log("%s\n", "Length more than 4 is not supported\n");
      return;
   }
   // the first string wont be used.
   char *single_digit[] = { "num0", "num1", "num2", "num3", "num4","num5", "num6", "num7", "num8", "num9"};
   
      char *tens_place[] = {"", "num10", "num11", "num12", "num13", "num14", "num15", "num16", "num17", "num18", "num19"};
   
      char *tens_multiple[] = {"", "", "num20", "num30", "num40", "num50","num60", "num70", "num80", "num90"};
      char *tens_power[] = {"unit0", "unit1"};
   
   // For single digit number
   if (len == 1) {
      char dig[10];
      strcpy(dig, single_digit[*num - '0']);
      strcat(dig, ".wav");
      printf("%s\n", dig);
	  DelayMs(1000);
	  SoundPlay(dig, 1);
	  DelayMs(1000);
      return;
   }
   // Iterate while num is not '\0'
   while (*num != '\0') {
      // Code path for first 2 digits
      if (len >= 3) {
         if (*num -'0' != 0) {
           
            char dig[10];
            strcpy(dig, single_digit[*num - '0']);
            strcat(dig, ".wav");
			DelayMs(1000);
			SoundPlay(dig,1);
			DelayMs(1000);
            printf("%s ", dig);
            strcpy(dig,tens_power[len-3]);
            strcat(dig, ".wav");
			DelayMs(1000);
			SoundPlay(dig,1);
			DelayMs(1000);
            printf("%s ", dig);
         }
         --len;
      }
      // Code path for last 2 digits
      else {
         // Need to explicitly handle 10-19. Sum of the two digits is
         //used as index of "tens_place" array of strings
         if (*num == '1') {
            int sum = *num - '0' + *(num + 1)- '0';
            char dig[10];
            strcpy(dig, tens_place[sum]);
            strcat(dig, ".wav");
            printf("%s ", dig);
			DelayMs(1000);
			SoundPlay(dig,1);
			DelayMs(1000);
            return;
         }
         // Need to explicitely handle 20
         else if (*num == '2' && *(num + 1) == '0') {
            //printf("twenty\n");
            char dig[10];
            strcpy(dig, "num20");
            strcat(dig, ".wav");
            printf("%s ", dig);
			DelayMs(1000);
			SoundPlay(dig,1);
			DelayMs(1000);
            return;
         }
         // Rest of the two digit numbers i.e., 21 to 99
         else {
            int i = *num - '0';
            char dig[10];
            if(i) {
              strcpy(dig, tens_multiple[i]);
              strcat(dig, ".wav");
              printf("%s ", dig);
			  DelayMs(1000);
			  SoundPlay(dig,1);
			  DelayMs(1000);
            }
           // printf("%s ", i? tens_multiple[i]: "");
            ++num;
            if (*num != '0')
              // printf("%s ", single_digit[*num - '0']);
				strcpy(dig, single_digit[*num - '0']);
				strcat(dig, ".wav");
				DelayMs(1000);
				SoundPlay(dig,1);
				DelayMs(1000);
				printf("%s ", dig);
         }
      }
      ++num;
   }
}

int main(void)
{
	unsigned char oldTime[8],curTime[8],str[8];
	volatile unsigned char ledindex;
	unsigned char portSet[20] = "115200,8,n,1";
	PortOpen(11, portSet);
	SysLedHandlerSet(LedSetCallback);
	
	ledindex = 0;
	SoundVolumeSet(4,0);
	while (value <= 199) 
	{	
		value += 1;
		sprintf(str, "%d", value);
		LedDisplay(1,str);
		int y = BatteryCheck();
		//Log("Battery: %d \r", y);
		strcpy(oldTime,curTime);
		DelayMs(1000);
		memset(curTime,0,sizeof(curTime));
		GetTime(curTime);
		
			memset(str,0,sizeof(str));
			
			//Log("Counter %d\n", value);
			int key =0; 
			key = GetKey();
			//Log("the key is %d\n", key);
			char floa[100];
	
			memset(floa, 0 , sizeof(floa));
			if(key == 43) {
				SoundVolumeSet(4,0);
				//int soundReturn1 = SoundPlay("num10.wav",1);
				//playDigit(10);
				//Log("Return value for sound %d\n", soundReturn1);
				int j=0;
				int i=0;
				strcpy(floa,"ADDING 10 TO ");
				char tempStr[10];
				sprintf(tempStr, "%d", value);
				strcat(floa,tempStr); 
				char ch=floa[0];
				int k = 0;	
				
				char num_convert[100];
  				sprintf(num_convert, "%d", value);
				convert(num_convert);


				scrolling(floa);

				// LedDisplay(1, "adding");
				// DelayMs(1000);
				// LedDisplay(1, "10 to");
				// DelayMs(1000);
				// char com[10];
				// sprintf(com, "%d", value);
				// LedDisplay(1, com);
				// DelayMs(1000);
				value += 10;
			}
			if(key == 45) {
				strcpy(floa,"MINUS 10 TO - ");
				char tempStr[10];
				sprintf(tempStr, "%d", value);
				strcat(floa,tempStr); 
				char ch=floa[0];
				int i=0;
				int j=0;

				scrolling(floa);  //function for scrolling

				value -=10;
				//Reboot();
			}
			if(key == 79) {
				int i=0;
				strcpy(floa,"FACTORS OF - ");
				char tempStr[10];
				sprintf(tempStr, "%d", value);
				strcat(floa,tempStr); 
				scrolling(floa);
				char temp[10];
				int rootValue = pow(value, 1/2);
				for(i=1; i<=value; i++)
				{
					if(value % i == 0){	
					sprintf(temp, "%d", i);
					LedDisplay(1, temp);
					DelayMs(1000); 
					}
				}
			}
			
			if(stopCounter ==1) {
				value = rand();
				break;
			}
		
	}
		return 0;
	
}

	int event_main(ST_EVENT_MSG *msg)
	{
		s_systemInit();
		return main();
	}
